/* AUTO-GENERATED FILE.  DO NOT MODIFY.
 *
 * This class was automatically generated by the
 * gradle plugin from the resource data it found. It
 * should not be modified by hand.
 */
package com.github.rahatarmanahmed.cpv;

public final class R {
    public static final class attr {
        public static final int cpv_animAutostart = 0x7f0100b9;
        public static final int cpv_animDuration = 0x7f0100b3;
        public static final int cpv_animSteps = 0x7f0100ba;
        public static final int cpv_animSwoopDuration = 0x7f0100b4;
        public static final int cpv_animSyncDuration = 0x7f0100b5;
        public static final int cpv_color = 0x7f0100b6;
        public static final int cpv_indeterminate = 0x7f0100b8;
        public static final int cpv_maxProgress = 0x7f0100b2;
        public static final int cpv_progress = 0x7f0100b1;
        public static final int cpv_startAngle = 0x7f0100bb;
        public static final int cpv_thickness = 0x7f0100b7;
    }
    public static final class bool {
        public static final int cpv_default_anim_autostart = 0x7f090005;
        public static final int cpv_default_is_indeterminate = 0x7f090006;
    }
    public static final class color {
        public static final int cpv_default_color = 0x7f0b001c;
    }
    public static final class dimen {
        public static final int cpv_default_thickness = 0x7f07005e;
    }
    public static final class integer {
        public static final int cpv_default_anim_duration = 0x7f0a0006;
        public static final int cpv_default_anim_steps = 0x7f0a0007;
        public static final int cpv_default_anim_swoop_duration = 0x7f0a0008;
        public static final int cpv_default_anim_sync_duration = 0x7f0a0009;
        public static final int cpv_default_max_progress = 0x7f0a000a;
        public static final int cpv_default_progress = 0x7f0a000b;
        public static final int cpv_default_start_angle = 0x7f0a000c;
    }
    public static final class string {
        public static final int app_name = 0x7f060026;
    }
    public static final class styleable {
        public static final int[] CircularProgressView = { 0x7f0100b1, 0x7f0100b2, 0x7f0100b3, 0x7f0100b4, 0x7f0100b5, 0x7f0100b6, 0x7f0100b7, 0x7f0100b8, 0x7f0100b9, 0x7f0100ba, 0x7f0100bb };
        public static final int CircularProgressView_cpv_animAutostart = 8;
        public static final int CircularProgressView_cpv_animDuration = 2;
        public static final int CircularProgressView_cpv_animSteps = 9;
        public static final int CircularProgressView_cpv_animSwoopDuration = 3;
        public static final int CircularProgressView_cpv_animSyncDuration = 4;
        public static final int CircularProgressView_cpv_color = 5;
        public static final int CircularProgressView_cpv_indeterminate = 7;
        public static final int CircularProgressView_cpv_maxProgress = 1;
        public static final int CircularProgressView_cpv_progress = 0;
        public static final int CircularProgressView_cpv_startAngle = 10;
        public static final int CircularProgressView_cpv_thickness = 6;
    }
}
