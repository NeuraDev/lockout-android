package com.lockout.keys;

import android.app.ActionBar;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.common.util.concurrent.FutureCallback;
import com.google.common.util.concurrent.Futures;
import com.google.common.util.concurrent.ListenableFuture;
import com.lockout.keys.activities.LoggedInActivity;
import com.lockout.keys.models.HeadersListModel;
import com.lockout.keys.models.SendObject;

/**
 * Created by sunwi on 21.04.2016.
 */
public class AskActivity extends AppCompatActivity {
    private Button mBtnOk;
    private Button mBtnNo;
    private ProgressBar mPbWait;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_ask);

        ActionBar mActionBar = getActionBar();
        mActionBar.setDisplayShowHomeEnabled(false);
        mActionBar.setDisplayShowTitleEnabled(false);
        LayoutInflater mInflater = LayoutInflater.from(this);

        View mCustomView = mInflater.inflate(R.layout.custom_actionbar, null);
        TextView mActionTitle = (TextView) mCustomView.findViewById(R.id.lblActionTitle);
        mActionTitle.setText("Вопрос");

        TextView mActionOk = (TextView) mCustomView.findViewById(R.id.lblActionOk);
        mActionOk.setText("");

        ImageView mImgMenu = (ImageView) mCustomView.findViewById(R.id.imgActionMenu);
        mImgMenu.setVisibility(View.INVISIBLE);

        mActionBar.setCustomView(mCustomView);
        mActionBar.setDisplayShowCustomEnabled(true);

        mBtnOk = (Button) findViewById(R.id.btnAskOk);
        mBtnNo = (Button) findViewById(R.id.btnAskNo);
        mPbWait = (ProgressBar) findViewById(R.id.pbAskWait);

        setAvailableUI(true);

        mBtnOk.setOnClickListener(okClickListener);
        mBtnNo.setOnClickListener(noClickListener);
    }

    View.OnClickListener okClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
//            setAvailableUI(false);
//            HeadersListModel hListModel = new HeadersListModel();
//            hListModel.deviceid = getSharedPreferences("UserData", 0).getString("deviceid", "");
//            final Pack pack = new Pack(getApplicationContext());
//            SendObject sendObject = pack.packing(hListModel);
//            ListenableFuture<SendObject> respond = mAuthService.getClient().invokeApi("tables/CleanKey", sendObject, "POST", null, SendObject.class);
//            Futures.addCallback(respond, new FutureCallback<SendObject>() {
//                @Override
//                public void onSuccess(SendObject result) {
//                    setAvailableUI(true);
//                    startActivity(new Intent(getApplicationContext(), LoggedInActivity.class));
//                }
//
//                @Override
//                public void onFailure(Throwable t) {
//                    setAvailableUI(true);
//                    Toast.makeText(getApplicationContext(), ErrorService.ParseError(t.getLocalizedMessage()), Toast.LENGTH_SHORT).show();
//                }
//            });
        }
        };

    private void setAvailableUI(boolean v) {
        mBtnNo.setEnabled(v);
        mBtnOk.setEnabled(v);
        mBtnOk.setClickable(v);
        mBtnNo.setClickable(v);
        mPbWait.setVisibility(v ? View.GONE : View.VISIBLE);
    }

    View.OnClickListener noClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            startActivity(new Intent(getApplicationContext(), LoggedInActivity.class));
        }
    };
}
