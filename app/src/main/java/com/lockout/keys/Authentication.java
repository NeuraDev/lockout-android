package com.lockout.keys;

import android.util.Base64;
import android.util.Log;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Random;

import static java.math.BigInteger.valueOf;

/**
 * Created by sunwi on 07.12.2015.
 */
public class Authentication {
    public BigInteger a;
    public BigInteger A;
    public BigInteger N;
    public BigInteger g = valueOf(2);

    static byte[] makeUnsigned(byte[] input) {
        if ((input[input.length - 1] & 0x80) > 0) {
            byte[] temp = new byte[input.length];
            System.arraycopy(input, 0, temp, 0, input.length);
            input = new byte[temp.length + 1];
            System.arraycopy(temp, 0, input, 0, temp.length);
        }

        return input;
    }

    public BigInteger RandomString() {
        int i = 0;
        String BigParam = "";
        Random rand = new Random();
        while (i < 64) {
            BigParam = BigParam + rand.nextInt(9);
            i = i + 1;
        }
        a = new BigInteger(BigParam);
        return a;
    }

    public static byte[] hexStringToByteArray(String s) {
        int len = s.length();
        byte[] data = new byte[len / 2];
        for (int i = 0; i < len; i += 2) {
            data[i / 2] = (byte) ((Character.digit(s.charAt(i), 16) << 4)
                    + Character.digit(s.charAt(i+1), 16));
        }
        return data;
    }

    public BigInteger getN(String username, String salt) {
        byte[][] S = new byte[8][];
        S[0] = hashAlg1(username);
        S[1] = hashAlg1(username + "1");
        S[2] = hashAlg1(username + salt);
        S[3] = hashAlg1(username + salt + "1");
        S[4] = hashAlg1(username + "2");
        S[5] = hashAlg1(username + salt + "2");
        S[6] = hashAlg1(username + "5");
        S[7] = hashAlg1(username + salt + "5");
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream( );
        for(int i = 0; i < 8; i++) {
            try {
                outputStream.write(S[i]);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        byte[] result = outputStream.toByteArray();
        result = makeUnsigned(result);

        for(int i = 0; i < result.length / 2; i++)
        {
            byte temp = result[i];
            result[i] = result[result.length - i - 1];
            result[result.length - i - 1] = temp;
        }

        N = new BigInteger(result);
        if (N.mod(BigInteger.valueOf(2)).equals(BigInteger.valueOf(0))){
            N = N.add(BigInteger.valueOf(1));
        }

        if (N.signum() < 0)
        N = N.negate();

        return N;
    }

    final protected static char[] hexArray = "0123456789ABCDEF".toCharArray();
    public static String bytesToHex(byte[] bytes) {
        char[] hexChars = new char[bytes.length * 2];
        for ( int j = 0; j < bytes.length; j++ ) {
            int v = bytes[j] & 0xFF;
            hexChars[j * 2] = hexArray[v >>> 4];
            hexChars[j * 2 + 1] = hexArray[v & 0x0F];
        }
        return new String(hexChars);
    }

    public byte[] hashAlg1(String str) {
        MessageDigest md = null;
        try {
            md = MessageDigest.getInstance("SHA-512");
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        if (md != null) {
            md.update(str.getBytes());
            byte byteData[] = md.digest();
            return byteData;
        }
        return str.getBytes();
    }

    public BigInteger getA() {
            A = g.modPow(a, N);
        return A;
    }

    public BigInteger u;

    public BigInteger getU(String B) {
        String ab = A.toString() + B;
        byte[] byteArray = hashAlg1(ab);

        byteArray = makeUnsigned(byteArray);
        for(int i = 0; i < byteArray.length / 2; i++)
        {
            byte temp = byteArray[i];
            byteArray[i] = byteArray[byteArray.length - i - 1];
            byteArray[byteArray.length - i - 1] = temp;
        }

        u = new BigInteger(byteArray);

        if (u.signum() < 0) {
            u = u.negate();
        }
        return u;
    }

    public BigInteger k;

    public BigInteger getk() {
        String Ng = N.toString() + g.toString();
        byte[] byteArray = hashAlg1(Ng);

        byteArray = makeUnsigned(byteArray);

        for(int i = 0; i < byteArray.length / 2; i++)
        {
            byte temp = byteArray[i];
            byteArray[i] = byteArray[byteArray.length - i - 1];
            byteArray[byteArray.length - i - 1] = temp;
        }

        k = new BigInteger(byteArray);
        if (k.signum() < 0){
            k = k.negate();
        }
        return k;
    }

    public static String encryptPassword(String password) {
        MessageDigest md = null;
        try {
            md = MessageDigest.getInstance("SHA-512");
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        if (md != null) {
            md.update(password.getBytes());
            byte byteData[] = md.digest();
            String base64 = Base64.encodeToString(byteData, Base64.DEFAULT);
            return base64;
        }
        return password;
    }

    public BigInteger getX(String getStringPassword, String salt) {
        BigInteger x = BigInteger.valueOf(0);
        String s = salt + getStringPassword;
        String hashString = encryptPassword(s);
        byte[] hashByte = Base64.decode(hashString, Base64.DEFAULT);

        hashByte = makeUnsigned(hashByte);
        for(int i = 0; i < hashByte.length / 2; i++)
        {
            byte temp = hashByte[i];
            hashByte[i] = hashByte[hashByte.length - i - 1];
            hashByte[hashByte.length - i - 1] = temp;
        }

        x = new BigInteger(hashByte);

        Log.e("x", "This is x - " + x.toString());

        if (x.signum() < 0) {
            x = x.negate();
        }
        return x;
    }

    public BigInteger S;

    public BigInteger getS(String B, String g, String x){
        //Log.e("Digits", B + " " + g + " " + x);
        BigInteger xtemp = new BigInteger(x);
        BigInteger Btemp = new BigInteger(B);
        BigInteger gtemp = new BigInteger(g);
        BigInteger S0 = a.add(u.multiply(xtemp));
        BigInteger S1 = Btemp.subtract(k.multiply(gtemp.modPow(xtemp, N)));
        S = S1.modPow(S0, N);

        return S;
    }

    public BigInteger K;

    public BigInteger getK() {
        byte[] byteArray = hashAlg1(S.toString());

        byteArray = makeUnsigned(byteArray);
        for(int i = 0; i < byteArray.length / 2; i++)
        {
            byte temp = byteArray[i];
            byteArray[i] = byteArray[byteArray.length - i - 1];
            byteArray[byteArray.length - i - 1] = temp;
        }

        K = new BigInteger(byteArray);

        return K;
    }

    public BigInteger M;

    public BigInteger getM(BigInteger N, BigInteger g, String I, String s, BigInteger A, String B, BigInteger K) {
        byte[] hashIByte = hashAlg1(I);

        hashIByte = makeUnsigned(hashIByte);
        for(int i = 0; i < hashIByte.length / 2; i++)
        {
            byte temp = hashIByte[i];
            hashIByte[i] = hashIByte[hashIByte.length - i - 1];
            hashIByte[hashIByte.length - i - 1] = temp;
        }

        BigInteger hashBigIntI = new BigInteger(hashIByte);

        byte[] byteArrayForHashN = hashAlg1(N.toString());
        byte[] byteArrayForHashg = hashAlg1(g.toString());

        byteArrayForHashg = makeUnsigned(byteArrayForHashg);
        byteArrayForHashN = makeUnsigned(byteArrayForHashN);

        for(int i = 0; i < byteArrayForHashN.length / 2; i++)
        {
            byte temp = byteArrayForHashN[i];
            byteArrayForHashN[i] = byteArrayForHashN[byteArrayForHashN.length - i - 1];
            byteArrayForHashN[byteArrayForHashN.length - i - 1] = temp;
        }

        for(int i = 0; i < byteArrayForHashg.length / 2; i++)
        {
            byte temp = byteArrayForHashg[i];
            byteArrayForHashg[i] = byteArrayForHashg[byteArrayForHashg.length - i - 1];
            byteArrayForHashg[byteArrayForHashg.length - i - 1] = temp;
        }

        BigInteger NHash = new BigInteger(byteArrayForHashN);
        BigInteger gHash = new BigInteger(byteArrayForHashg);

        byte[] byteArray = hashAlg1(NHash.xor(gHash).toString() + hashBigIntI.toString() + s + A.toString() + B + K.toString());

        byteArray = makeUnsigned(byteArray);
        for(int i = 0; i < byteArray.length / 2; i++)
        {
            byte temp = byteArray[i];
            byteArray[i] = byteArray[byteArray.length - i - 1];
            byteArray[byteArray.length - i - 1] = temp;
        }

        M = new BigInteger(byteArray);
        return M;
    }

    public BigInteger R;

    public BigInteger getR(BigInteger A, BigInteger M, BigInteger K) {
        String str = A.toString() + M.toString() + K.toString();
        byte[] byteArray = hashAlg1(str);
        byteArray = makeUnsigned(byteArray);

        for(int i = 0; i < byteArray.length / 2; i++)
        {
            byte temp = byteArray[i];
            byteArray[i] = byteArray[byteArray.length - i - 1];
            byteArray[byteArray.length - i - 1] = temp;
        }

        R = new BigInteger(byteArray);

        Log.e("R", "This is R - " + M.toString());
        return R;
    }

    public BigInteger getV(String x) {
        BigInteger tempx = new BigInteger(x);
        BigInteger result = g.modPow(tempx, N);
        Log.e("TAG", "V - " + result.toString() + " x - " + tempx.toString() + " g - " + g.toString() + " N - " + N.toString());
        return result;
    }
}
