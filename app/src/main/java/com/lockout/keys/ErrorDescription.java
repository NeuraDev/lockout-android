package com.lockout.keys;

import com.lockout.keys.models.AbstractPacket;

/**
 * Created by sunwi on 21.04.2016.
 */
public class ErrorDescription extends AbstractPacket {
    public String Status;
    public String Description;
}
