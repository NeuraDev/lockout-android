package com.lockout.keys;

import android.util.Log;

/**
 * Created by sunwi on 20.04.2016.
 */
public class ErrorService {
    public static String ParseError(String e) {
        if (e.equals("666")) {
            return "В данный момент оплата невозможна";
        }
        if (e.equals("{'code': 409}")) {
            return "Данный e-mail уже существует";
        }
        if (e.equals("{'code': 410}")) {
            return "Gone";
        }
        if (e.equals("{'code': 406}")) {
            return "Вы превысили бесплатный лимит на 20 записей. Пожалуйста, оплатите годовую подписку в Настройках профиля, либо удалите одну из записей";
        }
        if (e.equals("{'code': 403}")) {
            return "Отказано в доступе";
        }
        if (e.equals("{'code': 500}")) {
            return "Произошла внутренняя ошибка сервера. Попробуйте еще раз";
        }
        if (e.equals("{'code': 400}")) {
            return "Произошла внутренняя ошибка клиента. Попробуйте еще раз";
        }
        if (e.equals("{'code': 401}")) {
            return "Отказано в доступе";
        }
        if (e.equals("Error while processing request")) {
            return "Произошла внутренняя ошибка клиента. Перезапустите приложение и попробуйте еще раз";
        }
        Log.e("Error", e);
        return "Произошла внутренняя ошибка клиента. Попробуйте еще раз";
    }
}
