package com.lockout.keys;

import android.app.ActionBar;
import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.common.util.concurrent.FutureCallback;
import com.google.common.util.concurrent.Futures;
import com.google.common.util.concurrent.ListenableFuture;
import com.lockout.keys.models.Account;

/**
 * Created by sunwi on 08.04.2016.
 */
public class GridActivity extends Activity {
    private EditText email;
    private Button btnNext;
    private Button btnBack;
    private ProgressBar pbWait;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_restore);

        ActionBar mActionBar = getActionBar();
        mActionBar.setDisplayShowHomeEnabled(false);
        mActionBar.setDisplayShowTitleEnabled(false);
        LayoutInflater mInflater = LayoutInflater.from(this);

        View mCustomView = mInflater.inflate(R.layout.custom_actionbar, null);
        TextView mActionTitle = (TextView) mCustomView.findViewById(R.id.lblActionTitle);
        mActionTitle.setText("Таблица восстановления");

        TextView mActionOk = (TextView) mCustomView.findViewById(R.id.lblActionOk);
        mActionOk.setText("Готово     ");
        mActionOk.setOnClickListener(nextClickListener);

        ImageView mImgMenu = (ImageView) mCustomView.findViewById(R.id.imgActionMenu);
        mImgMenu.setOnClickListener(cancelClickListener);

        mActionBar.setCustomView(mCustomView);
        mActionBar.setDisplayShowCustomEnabled(true);

        findViewById(R.id.rlRestore).setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
                return false;
            }
        });

        email = (EditText) findViewById(R.id.txtLoginEmail);
        btnNext = (Button) findViewById(R.id.btnProceed);
        btnBack = (Button) findViewById(R.id.btnCancel);
        pbWait = (ProgressBar) findViewById(R.id.pbLogin);

        setAvailableUI(true);

        email.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                boolean handled = false;
                if (actionId == EditorInfo.IME_ACTION_GO) {
                  /* Write your logic here that will be executed when user taps next button */
                    btnNext.performClick();
                    handled = true;
                }
                return handled;
            }
        });

        btnNext.setOnClickListener(nextClickListener);
        btnBack.setOnClickListener(cancelClickListener);
    }

    View.OnClickListener nextClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            setAvailableUI(false);
            Account acc = new Account();
            acc.email = email.getText().toString();
//            ListenableFuture<String> respond = mAuthService.getClient().invokeApi("tables/Grid", acc, "POST", null, String.class);
//            Futures.addCallback(respond, new FutureCallback<String>() {
//                @Override
//                public void onSuccess(String result) {
//                    setAvailableUI(true);
//                    Toast.makeText(getApplicationContext(), result + ". Введите код из таблицы для входа", Toast.LENGTH_SHORT).show();
//                    email.setText(result);
//                    TextView logo = (TextView) findViewById(R.id.lblLoginEmail);
//                    logo.setText("Ваш код:");
//                    //startActivity(new Intent(getApplicationContext(), LoginActivity.class));
//                }
//
//                @Override
//                public void onFailure(Throwable t) {
//                    setAvailableUI(true);
//                    Toast.makeText(getApplicationContext(), ErrorService.ParseError(t.getLocalizedMessage()), Toast.LENGTH_SHORT).show();
//                }
//            });
        }
    };

    private void setAvailableUI(boolean v) {
        btnNext.setEnabled(v);
        btnNext.setClickable(v);
        btnBack.setEnabled(v);
        btnBack.setClickable(v);
        email.setEnabled(v);
        email.setClickable(v);
        if (v) {
            pbWait.setVisibility(View.INVISIBLE);
        } else {
            pbWait.setVisibility(View.VISIBLE);
        }
    }

    View.OnClickListener cancelClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            finish();
        }
    };
}
