package com.lockout.keys;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;

import com.lockout.keys.models.Model;

import java.util.ArrayList;


/**
 * Created by sunwi on 10.12.2015.
 */
public class MyArrayAdapter extends ArrayAdapter<Model> implements Filterable {
    private final Activity context ;
    private ArrayList<Model> data;
    private ArrayList<Model> filtered;

    public MyArrayAdapter(Activity context, ArrayList<Model> data) {
        super(context, R.layout.rowlayout, data);
        this.context = context;
        this.data = data;
        this.filtered = data;
    }

    static class ViewHolder {
        public ImageView logo;
        public TextView header;
        public ImageView sublogo;
        public TextView subheader;
        public TextView type;
    }

    @Override
    public int getCount() {
        return filtered.size();
    }

    @Override
    public Model getItem(int position) {
        return filtered.get(position);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        View view = null;

        if (convertView == null) {
            LayoutInflater inflater = context.getLayoutInflater();
            view = inflater.inflate(R.layout.rowlayout, parent, false);
            holder = new ViewHolder();
            holder.header = (TextView) view.findViewById(R.id.labelRow);
            holder.logo = (ImageView) view.findViewById(R.id.iconRow);
            holder.subheader = (TextView) view.findViewById(R.id.sublabelRow);
            holder.sublogo = (ImageView) view.findViewById(R.id.secureRow);
            holder.type = (TextView) view.findViewById(R.id.typeRow);
            view.setTag(holder);
        } else {
            view = convertView;
            holder = (ViewHolder) view.getTag();
        }

        holder.header.setText(filtered.get(position).getName());
        holder.subheader.setText(filtered.get(position).getSubname());
        holder.type.setText(filtered.get(position).getType());
            if (filtered.get(position).getSubname().equals("Защищенный")) {
                holder.sublogo.setImageResource(R.drawable.strongest);
            } else if (filtered.get(position).getSubname().equals("Усиленный")) {
                holder.sublogo.setImageResource(R.drawable.strong);
            } else if (filtered.get(position).getSubname().equals("Обычный")) {
                holder.sublogo.setImageResource(R.drawable.simple);
            } else {
                holder.sublogo.setVisibility(View.INVISIBLE);
            }
            return view;
    }

    @Override
    public Filter getFilter() {
        return new Filter() {

            @Override
            protected FilterResults performFiltering(CharSequence constraint) {
                FilterResults results = new FilterResults();
                ArrayList<Model> FilteredArray = new ArrayList<Model>();
                String consta = constraint.toString().toLowerCase();
                for (int i = 0; i < data.size(); i++) {
                    String datanames = data.get(i).getName();
                    if (datanames.toLowerCase().contains(consta)) {
                        FilteredArray.add(data.get(i));
                    }
                }

                results.count = FilteredArray.size();
                results.values = FilteredArray;

                return results;
            }

            @Override
            protected void publishResults(CharSequence constraint, FilterResults results) {
                filtered = (ArrayList<Model>) results.values;
                notifyDataSetChanged();
            }
        };
    }
}
