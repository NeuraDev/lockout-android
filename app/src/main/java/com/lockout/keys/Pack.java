package com.lockout.keys;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Base64;
import android.util.Log;
import android.widget.Toast;

import com.google.common.util.concurrent.ListenableFuture;
import com.google.gson.FieldNamingPolicy;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;
import com.lockout.keys.encrpytion.Encryptor;
import com.lockout.keys.models.AbstractPacket;
import com.lockout.keys.models.DataObject;
import com.lockout.keys.models.SendObject;
import com.microsoft.windowsazure.mobileservices.MobileServiceClient;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;

import java.io.StringReader;
import java.io.UnsupportedEncodingException;
import java.math.BigInteger;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

/**
 * Created by sunwi on 12.12.2015.
 */
public class Pack {
    private final String TAG = "PACK";
    private Context mContext;
    private MobileServiceClient mClient;
    private int flag = 0;

    public Pack (Context context) {
        this.mContext = context;
    }

    private static Document loadXMLFromString(String xml) throws Exception
    {
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        DocumentBuilder builder = factory.newDocumentBuilder();
        InputSource is = new InputSource(new StringReader(xml));
        return builder.parse(is);
    }

    public <T extends AbstractPacket> JsonObject packing(T input) {
        SendObject sendObject = new SendObject();
        SimpleDateFormat dateFormatUTC = new SimpleDateFormat("dd.MM.yyyy HH:mm:ss");
        input.dateSend = dateFormatUTC.format(new Date());
        Gson gson = new Gson();
        String json = gson.toJson(input);
        SecureRandom random = new SecureRandom();
        String AESKey = Base64.encodeToString(new BigInteger(255, random).toByteArray(), Base64.DEFAULT);
        AESKey = AESKey.replace("\n", "").replace("\r", "");
        SharedPreferences settings = mContext.getSharedPreferences("UserData", 0);
        String iv = settings.getString("sendIv", "");
        String encrypted = Encryptor.encrypt(AESKey, iv, json);
        encrypted = encrypted.replace("\n", "").replace("\r", "");

        String sok = settings.getString("serverOpenKey", null);

        if (sok != null && sok != "") {

            String temp = null;
            try {
                temp = new String(Base64.decode(sok, Base64.DEFAULT), "UTF-8");
            } catch (Exception e) {
                Log.e("Error", e.getLocalizedMessage());
            }

            Document xml = null;
            try {
                xml = loadXMLFromString(temp);
            } catch (Exception e) {
                Log.e("Error", e.getLocalizedMessage());
            }
            Element root = xml.getDocumentElement();
            NodeList modList = root.getElementsByTagName("Modulus");
            NodeList expList = root.getElementsByTagName("Exponent");
            NodeList modNode = modList.item(0).getChildNodes();
            NodeList exponentNode = expList.item(0).getChildNodes();

            String mod = modNode.item(0).getNodeValue();
            String exponent = exponentNode.item(0).getNodeValue();

            try{
                sendObject.aeskey = Encryptor.RSAEncrypt(AESKey, mod, exponent);
            } catch (Exception e) {
                Log.e("Error - ", e.getLocalizedMessage());
            }
        } else {
            sendObject.aeskey =  AESKey;
        }

        sendObject.aesenc = encrypted;
        sendObject.iddevice = settings.getString("deviceid", "");
        sendObject.token = getToken(settings.getString("deviceid", ""), sendObject.aeskey, sendObject.aesenc, settings.getString("tokenpart", ""));
        sendObject.token = sendObject.token.replace("\n", "").replace("\r", "");
        return (JsonObject) new Gson().toJsonTree(sendObject);
    }

    public Date removeTime(Date date) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        cal.set(Calendar.HOUR_OF_DAY, 0);
        cal.set(Calendar.MINUTE, 0);
        cal.set(Calendar.SECOND, 0);
        cal.set(Calendar.MILLISECOND, 0);
        return cal.getTime();
    }

    public <T extends AbstractPacket> T unpacking(JsonObject input, Class<T> type) {
        SendObject source = new Gson().fromJson(input, SendObject.class);
        SharedPreferences settings = mContext.getSharedPreferences("UserData", 0);
        String serverToken = getToken(settings.getString("deviceid", ""), source.aeskey, source.aesenc, settings.getString("tokenpart", ""));
        serverToken = serverToken.replace("\n", "").replace("\r", "");
        if (!serverToken.equals(source.token)) {
            Toast.makeText(mContext, mContext.getString(R.string.error_channel), Toast.LENGTH_SHORT).show();
            return null;
        }
        String key = null;
        try {
            key = Encryptor.RSADecrypt(settings.getString("clientKeyPair", ""), source.aeskey);
        } catch (Exception e) {
            Log.e("Error - ", e.getLocalizedMessage());
        }
        String iv = settings.getString("sendIv", "");
        String base64decrypted = Encryptor.decrypt(key, iv, source.aesenc);

        Gson gson = new GsonBuilder().setFieldNamingPolicy(FieldNamingPolicy.IDENTITY).create();
        T serialized = gson.fromJson(base64decrypted, type);
        SimpleDateFormat dateFormatUTC = new SimpleDateFormat("dd.MM.yyyy HH:mm:ss");

        //if (removeTime(new Date()).equals()) Сделать проверку на дату

        return serialized;
    }

    private static String getToken(String idDevice, String encryptedAesKey, String encryptedData, String R) {
        String summary = idDevice + encryptedAesKey + encryptedData + R;
        String hash = Authentication.encryptPassword(summary);
        return hash;
    }

}
