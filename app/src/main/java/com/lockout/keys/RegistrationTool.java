package com.lockout.keys;

import android.util.Base64;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Random;

/**
 * Created by sunwi on 14.12.2015.
 */
public class RegistrationTool {
    public BigInteger x;
    public BigInteger N;
    public String s = "";
    public BigInteger g = BigInteger.valueOf(2);

    public String RandomString() {
        int i = 0;
        String BigParam = "";
        Random rand = new Random();
        while (i < 64) {
            BigParam = BigParam + rand.nextInt(9);
            i = i + 1;
        }
        return BigParam;
    }

    public BigInteger getX(String getStringPassword) {
        s = RandomString();
        String s1 = s + getStringPassword;
        String hashString = encryptPassword(s1);

        byte[] hashByte = Base64.decode(hashString, Base64.DEFAULT);
        hashByte = Authentication.makeUnsigned(hashByte);

        for(int i = 0; i < hashByte.length / 2; i++)
        {
            byte temp = hashByte[i];
            hashByte[i] = hashByte[hashByte.length - i - 1];
            hashByte[hashByte.length - i - 1] = temp;
        }

        x = new BigInteger(hashByte);

        if (x.signum() < 0) {
            x = x.negate();
        }

        return x;
    }

    public static String encryptPassword(String password) {
        MessageDigest md = null;
        try {
            md = MessageDigest.getInstance("SHA-512");
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        if (md != null) {
            md.update(password.getBytes());
            byte byteData[] = md.digest();
            String base64 = Base64.encodeToString(byteData, Base64.DEFAULT);
            return base64;
        }
        return password;
    }

    public BigInteger getN(String username) {
        byte[][] S = new byte[8][];
        S[0] = hashAlg1(username);
        S[1] = hashAlg1(username + "1");
        S[2] = hashAlg1(username + s);
        S[3] = hashAlg1(username + s + "1");
        S[4] = hashAlg1(username + "2");
        S[5] = hashAlg1(username + s + "2");
        S[6] = hashAlg1(username + "5");
        S[7] = hashAlg1(username + s + "5");
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream( );
        for(int i = 0; i < 8; i++) {
            try {
                outputStream.write(S[i]);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        byte[] result = outputStream.toByteArray();
        result = Authentication.makeUnsigned(result);

        for(int i = 0; i < result.length / 2; i++)
        {
            byte temp = result[i];
            result[i] = result[result.length - i - 1];
            result[result.length - i - 1] = temp;
        }

        N = new BigInteger(result);

        if (N.mod(BigInteger.valueOf(2)).equals(BigInteger.valueOf(0))){
            N = N.add(BigInteger.valueOf(1));
        }

        if (N.signum() < 0)
            N = N.negate();
        return N;
    }

    public byte[] hashAlg1(String str) {
        MessageDigest md = null;
        try {
            md = MessageDigest.getInstance("SHA-512");
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        if (md != null) {
            md.update(str.getBytes());
            byte byteData[] = md.digest();
            return byteData;
        }
        return str.getBytes();
    }

    public BigInteger getV() {
        BigInteger result = g.modPow(x, N);
        return result;
    }
}
