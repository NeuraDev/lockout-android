package com.lockout.keys;

import android.app.ActionBar;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.github.rahatarmanahmed.cpv.CircularProgressView;
import com.google.common.util.concurrent.FutureCallback;
import com.google.common.util.concurrent.Futures;
import com.google.common.util.concurrent.ListenableFuture;
import com.google.gson.JsonObject;
import com.lockout.keys.activities.LoginActivity;
import com.lockout.keys.helpers.Utilities;
import com.lockout.keys.http.ApiName;
import com.lockout.keys.http.RetrofitApi;
import com.lockout.keys.models.Account;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

/**
 * Created by sunwi on 08.04.2016.
 */
public class SMSActivity extends AppCompatActivity {
    private EditText email;
    private Button btnNext;
    private Button btnBack;
    private CircularProgressView pbWait;
    private RetrofitApi.PostJson mApiCall;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_restore);

        email = (EditText) findViewById(R.id.txtLoginEmail);
        btnNext = (Button) findViewById(R.id.btnProceed);
        btnBack = (Button) findViewById(R.id.btnCancel);
        pbWait = (CircularProgressView) findViewById(R.id.pbLogin);

        Retrofit client = new RetrofitApi(getApplicationContext()).getRetrofitApi();
        mApiCall = client.create(RetrofitApi.PostJson.class);

        Utilities.setupHideKeyboard(this, findViewById(R.id.rlRestore));

        setAvailableUI(true);

        email.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                boolean handled = false;
                if (actionId == EditorInfo.IME_ACTION_GO) {
                  /* Write your logic here that will be executed when user taps next button */
                    btnNext.performClick();
                    handled = true;
                }
                return handled;
            }
        });

        btnNext.setOnClickListener(nextClickListener);
        btnBack.setOnClickListener(cancelClickListener);
    }

    View.OnClickListener nextClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            setAvailableUI(false);
            Account acc = new Account();
            acc.email = email.getText().toString();
//            mApiCall.post(ApiName.RESTORE_SMS, acc).enqueue(new Callback<JsonObject>() {
//                @Override
//                public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
//
//                }
//
//                @Override
//                public void onFailure(Call<JsonObject> call, Throwable t) {
//
//                }
//            });
//            ListenableFuture<String> respond = mAuthService.getClient().invokeApi("tables/SMS", acc, "POST", null, String.class);
//            Futures.addCallback(respond, new FutureCallback<String>() {
//                @Override
//                public void onSuccess(String result) {
//                    setAvailableUI(true);
//                    Toast.makeText(getApplicationContext(), result + ". Введите его вместо кодового слова для получения доступа", Toast.LENGTH_SHORT).show();
//                    startActivity(new Intent(getApplicationContext(), LoginActivity.class));
//                }
//
//                @Override
//                public void onFailure(Throwable t) {
//                    setAvailableUI(true);
//                    Toast.makeText(getApplicationContext(), ErrorService.ParseError(t.getLocalizedMessage()), Toast.LENGTH_SHORT).show();
//                }
//            });
        }
    };

    private void setAvailableUI(boolean v) {
        btnNext.setEnabled(v);
        btnNext.setClickable(v);
        btnBack.setEnabled(v);
        btnBack.setClickable(v);
        email.setEnabled(v);
        email.setClickable(v);
        if (v) {
            pbWait.setVisibility(View.INVISIBLE);
        } else {
            pbWait.setVisibility(View.VISIBLE);
        }
    };

    View.OnClickListener cancelClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            finish();
        }
    };
}
