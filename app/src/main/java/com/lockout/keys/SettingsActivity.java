package com.lockout.keys;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.ClipboardManager;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.vending.billing.IInAppBillingService;
import com.anjlab.android.iab.v3.BillingProcessor;
import com.anjlab.android.iab.v3.TransactionDetails;
import com.github.rahatarmanahmed.cpv.CircularProgressView;
import com.google.gson.JsonObject;
import com.lockout.keys.activities.ChangePasswordActivity;
import com.lockout.keys.activities.CheckUserActivity;
import com.lockout.keys.activities.LoggedInActivity;
import com.lockout.keys.helpers.AppData;
import com.lockout.keys.helpers.Utilities;
import com.lockout.keys.http.ApiName;
import com.lockout.keys.http.RetrofitApi;
import com.lockout.keys.models.Account;
import com.lockout.keys.models.Auth1Output;
import com.lockout.keys.models.CheckAndroid;

import java.text.SimpleDateFormat;

import es.dmoral.toasty.Toasty;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

/**
 * Created by sunwi on 05.12.2015.
 */
public class SettingsActivity extends AppCompatActivity implements BillingProcessor.IBillingHandler {
    private final static String SERVICE_INTENT = "com.android.vending.billing.InAppBilling.BIND";
    static final String SKU_1YEAR = "1yearsubscribe";
    static final String TAG = "SettingsActivity";
    private final String _testSku = "android.test.purchased";

    private EditText mTxtEmail;
    private EditText mTxtPassword;
    private EditText mTxtConfirm;
    private EditText mTxtUsername;
    private EditText mTxtPhone;

    private TextView mLblDelete;
    private TextView mBtnSave;

    String oldemail;
    String oldpassword;
    String oldphone;
    String oldusername;

    private Activity mActivity;

    private EditText mTxtOTP;
    private ImageView mIvOTP;
    private TextView mTxtGridLink;
    private TextView mLblGridLink;

    private String GridLink;
    private IInAppBillingService mService;
    private TextView mBtnPurchase;
    private BillingProcessor bp;
    private CircularProgressView mCpvWait;
    private RetrofitApi.PostJson mApiCall;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_settings);

        //Billing Service
        bp = new BillingProcessor(this, getString(R.string.token_billing), this);

        Toolbar toolbar = (Toolbar) findViewById(R.id.tb_settings_activity);
        setSupportActionBar(toolbar);

        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }

        Utilities.setupHideKeyboard(this, findViewById(R.id.cl_settings));
        Utilities.setupHideKeyboard(this, findViewById(R.id.nested_settings));

        mActivity = this;

        Retrofit client = new RetrofitApi(getApplicationContext()).getRetrofitApi();
        mApiCall = client.create(RetrofitApi.PostJson.class);

        mTxtConfirm = (EditText) findViewById(R.id.txtSettingsConfirm);
        mTxtEmail = (EditText) findViewById(R.id.txtSettingsEmail);
        mTxtPassword = (EditText) findViewById(R.id.txtSettingsPassword);
        mTxtPhone = (EditText) findViewById(R.id.txtSettingsPhone);
        mTxtUsername = (EditText) findViewById(R.id.txtSettingsUsername);
        mTxtOTP = (EditText) findViewById(R.id.txtSettingsOTP);
        mTxtGridLink = (TextView) findViewById(R.id.txtSettingsGridLink);
        mBtnPurchase = (TextView) findViewById(R.id.btnBuySubscribe);

        mIvOTP = (ImageView) findViewById(R.id.ivSettingsCopy);
        mIvOTP.setOnClickListener(copyotplistener);
        mTxtOTP.setClickable(false);
        mTxtOTP.setEnabled(false);
        mTxtGridLink.setOnClickListener(urlclicklistener);

        mBtnSave = (Button) findViewById(R.id.btnSettingsSave);
        mLblDelete = (TextView) findViewById(R.id.lblSettingsDelete);
        mLblGridLink = (TextView) findViewById(R.id.lblSettingsGridLink);

        mCpvWait = (CircularProgressView) findViewById(R.id.cpv_settings_wait);
        mBtnSave.setOnClickListener(saveClickListener);

        mTxtPassword.setOnClickListener(clearClickListener);
        mLblDelete.setOnClickListener(deleteClickListener);
        mBtnPurchase.setOnClickListener(buyClickListener);

        loadData();
    }

    @Override public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                break;

            default:
                break;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (!bp.handleActivityResult(requestCode, resultCode, data)) {
            super.onActivityResult(requestCode, resultCode, data);
        }
    }

    @Override protected void onDestroy() {
        if (bp != null)
            bp.release();

        super.onDestroy();
    }

    View.OnClickListener buyClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            boolean isAvailable = BillingProcessor.isIabServiceAvailable(getApplicationContext());
            if(isAvailable) {
                bp.subscribe(mActivity, SKU_1YEAR);
            } else {
                Toasty.error(getApplicationContext(), getString(R.string.buy_no_available), Toast.LENGTH_SHORT).show();
            }
        }
    };

    View.OnClickListener clearClickListener  = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            mTxtPassword.setText("");
            mTxtConfirm.setText("");
        }
    };

    View.OnClickListener copyotplistener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            ClipboardManager clipboard = (ClipboardManager) getSystemService(CLIPBOARD_SERVICE);
            clipboard.setText(mTxtOTP.getText().toString());
            Toast.makeText(getApplicationContext(), getString(R.string.copy_auth), Toast.LENGTH_SHORT).show();
        }
    };

    View.OnClickListener urlclicklistener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if (GridLink != null) {
                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(GridLink)));
            }
        }
    };

    View.OnClickListener deleteClickListener  = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            AppData.SOURCE_TYPE = AppData.SOURCE_DELETE;
            startActivity(new Intent(getApplicationContext(), ChangePasswordActivity.class));
        }
    };

    public void setAvailableUI(boolean v) {
        mTxtUsername.setEnabled(v);
        mTxtUsername.setClickable(v);
        mTxtConfirm.setEnabled(v);
        mTxtConfirm.setClickable(v);
        mTxtEmail.setEnabled(v);
        mTxtEmail.setClickable(v);
        mTxtPassword.setEnabled(v);
        mTxtPassword.setClickable(v);
        mTxtPhone.setEnabled(v);
        mTxtPhone.setClickable(v);
        mTxtGridLink.setEnabled(v);
        mTxtGridLink.setClickable(v);

        mBtnSave.setVisibility(v ? View.VISIBLE : View.GONE);
        mBtnPurchase.setVisibility(v ? View.VISIBLE : View.GONE);
        mCpvWait.setVisibility(v ? View.GONE : View.VISIBLE);
    }

    private void showGridLink(String input) {
        mTxtGridLink.setVisibility(!input.equals("") ? View.VISIBLE : View.GONE);
        mLblGridLink.setVisibility(!input.equals("") ? View.VISIBLE : View.GONE);
    }

    public void loadData() {
        setAvailableUI(false);
        final Pack pack = new Pack(getApplicationContext());
        Auth1Output auth1Output = new Auth1Output();
        auth1Output.id = AppData.USER_ID;

        mApiCall.post("tables/ReadSettings", pack.packing(auth1Output))
                .enqueue(new Callback<JsonObject>() {
                    @Override
                    public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                        if (response.code() == 200) {
                            Account account = pack.unpacking(response.body(), Account.class);
                            showGridLink(account.GridLink);
                            mTxtOTP.setText(account.SecretTOTP);
                            mTxtEmail.setText(account.email);
                            mTxtUsername.setText(account.username);
                            mTxtPhone.setText(account.phone);
                            mTxtPassword.setText("*******");
                            mTxtConfirm.setText("*******");

                            oldemail = account.email;
                            oldpassword = "*******";
                            oldphone = account.phone;
                            oldusername = account.username;
                            setAvailableUI(true);
                        } else {
                            setAvailableUI(true);
                            Toasty.error(getApplicationContext(), "Status code - " + response.code(), Toast.LENGTH_SHORT).show();
                            startActivity(new Intent(getApplicationContext(), LoggedInActivity.class));
                        }
                    }

                    @Override
                    public void onFailure(Call<JsonObject> call, Throwable t) {
                        setAvailableUI(true);
                        Toasty.error(getApplicationContext(), ErrorService.ParseError(t.getLocalizedMessage()), Toast.LENGTH_SHORT).show();
                        startActivity(new Intent(getApplicationContext(), LoggedInActivity.class));
                    }
                });
    }

    View.OnClickListener saveClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if (mTxtPassword.getText().toString().equalsIgnoreCase("") ||
                    mTxtPhone.getText().toString().equalsIgnoreCase("") ||
                    mTxtEmail.getText().toString().equalsIgnoreCase("") ||
                    mTxtUsername.getText().toString().equalsIgnoreCase("")) {
                Toasty.warning(getApplicationContext(), getString(R.string.info_all_fields), Toast.LENGTH_SHORT).show();
                return;
            }

            if (!mTxtPassword.getText().toString().equalsIgnoreCase(mTxtConfirm.getText().toString())) {
                Toasty.warning(getApplicationContext(), getString(R.string.info_passwords_break), Toast.LENGTH_SHORT).show();
                return;
            }

            if (!oldusername.equalsIgnoreCase(mTxtUsername.getText().toString()) || !oldphone.equalsIgnoreCase(mTxtPhone.getText().toString())) {
                setAvailableUI(false);
                final SharedPreferences settings = getSharedPreferences("UserData", 0);
                Account account = new Account();
                account.phone = mTxtPhone.getText().toString();
                account.username = mTxtUsername.getText().toString();
                account.id = AppData.USER_ID;

                Pack pack = new Pack(getApplicationContext());

                mApiCall.post(ApiName.UPDATE_USERNAME_PHONE, pack.packing(account))
                        .enqueue(new Callback<JsonObject>() {
                            @Override
                            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                                if (Utilities.validateCode(response.code())) {
                                    settings.edit().putString("username", mTxtUsername.getText().toString()).apply();
                                    Toasty.success(getApplicationContext(), getString(R.string.updated_account), Toast.LENGTH_SHORT).show();
                                    if (oldpassword.equalsIgnoreCase(mTxtPassword.getText().toString()) &&
                                            oldemail.equalsIgnoreCase(mTxtEmail.getText().toString())) {
                                        startActivity(new Intent(getApplicationContext(), LoggedInActivity.class));
                                    }
                                    setAvailableUI(true);
                                } else {
                                    Toasty.error(getApplicationContext(), "Status code - " + response.code(), Toast.LENGTH_SHORT).show();
                                    setAvailableUI(true);
                                }
                            }

                            @Override
                            public void onFailure(Call<JsonObject> call, Throwable t) {
                                Toasty.error(getApplicationContext(), ErrorService.ParseError(t.getLocalizedMessage()), Toast.LENGTH_SHORT).show();
                                setAvailableUI(true);
                            }
                        });
            }

            if (!oldpassword.equalsIgnoreCase(mTxtPassword.getText().toString()) ||
                    !oldemail.equalsIgnoreCase(mTxtEmail.getText().toString())) {
                startActivity(new Intent(getApplicationContext(), ChangePasswordActivity.class));
//                SharedPreferences settings = getSharedPreferences("UserData", 0);
//                SharedPreferences.Editor prefEdit = settings.edit();
//                prefEdit.putString("source", "changepsw");
//                prefEdit.putString("securepos", "2");
//                prefEdit.putString("oldemail", oldemail);
//                prefEdit.putString("newemail", mTxtEmail.getText().toString());
//                prefEdit.putString("newpassword", mTxtPassword.getText().toString());
//                prefEdit.commit();
//                Intent checkIntent = new Intent(getApplicationContext(), CheckUserActivity.class);
//                startActivity(checkIntent);
                return;
            }

            if (oldpassword.equalsIgnoreCase(mTxtPassword.getText().toString()) &&
                    oldemail.equalsIgnoreCase(mTxtEmail.getText().toString()) &&
                    oldusername.equalsIgnoreCase(mTxtUsername.getText().toString()) &&
                    oldphone.equalsIgnoreCase(mTxtPhone.getText().toString())) {
                startActivity(new Intent(getApplicationContext(), LoggedInActivity.class));
            }
        }
    };

    @Override public void onProductPurchased(String productId, TransactionDetails details) {
        setAvailableUI(false);
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd.MM.yyyy HH:mm:ss");
        CheckAndroid checkAndroid = new CheckAndroid();
        checkAndroid.orderId = details.orderId;
        checkAndroid.purchaseTime = dateFormat.format(details.purchaseTime);
        checkAndroid.purchaseInfo = details.purchaseInfo.responseData;
        checkAndroid.signature = details.purchaseInfo.signature;

        final Pack pack = new Pack(getApplicationContext());
        mApiCall.post(ApiName.BUY, pack.packing(checkAndroid))
                .enqueue(new Callback<JsonObject>() {
                    @Override
                    public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                        if (Utilities.validateCode(response.code())) {
                            ErrorDescription ret = pack.unpacking(response.body(), ErrorDescription.class);
                            Toasty.success(getApplicationContext(), ret.Description, Toast.LENGTH_SHORT).show();
                            setAvailableUI(true);
                        } else {
                            Toasty.error(getApplicationContext(), "Status code - " + response.code(), Toast.LENGTH_SHORT).show();
                            setAvailableUI(true);
                        }
                    }

                    @Override
                    public void onFailure(Call<JsonObject> call, Throwable t) {
                        Toasty.error(getApplicationContext(), ErrorService.ParseError(t.getLocalizedMessage()), Toast.LENGTH_SHORT).show();
                        setAvailableUI(true);
                    }
                });

    }

    @Override public void onPurchaseHistoryRestored() {}
    @Override public void onBillingInitialized() {}
    @Override public void onBillingError(int errorCode, Throwable error) {
        Toast.makeText(getApplicationContext(), ErrorService.ParseError("666"), Toast.LENGTH_SHORT).show();
        setAvailableUI(true);
    }
}
