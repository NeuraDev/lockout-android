package com.lockout.keys.activities;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.text.method.PasswordTransformationMethod;
import android.view.KeyEvent;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.github.rahatarmanahmed.cpv.CircularProgressView;
import com.google.gson.JsonObject;
import com.lockout.keys.ErrorService;
import com.lockout.keys.Pack;
import com.lockout.keys.R;
import com.lockout.keys.encrpytion.Encryptor;
import com.lockout.keys.helpers.Utilities;
import com.lockout.keys.http.RetrofitApi;
import com.lockout.keys.models.RelationDataModel;

import es.dmoral.toasty.Toasty;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AddNewDataActivity extends AppCompatActivity {
	private Activity mActivity;
	final String TAG = "AddNewDataActivity";

	private TextView mLblName;
	private TextView mLblLink;
	private TextView mLblLogin;
	private TextView mLblPassword;

	private EditText mTxtName;
	private EditText mTxtLink;
	private EditText mTxtLogin;
	private EditText mTxtPassword;

	private Spinner mSpSecure;
	private Button mBtnProcess;
	private ImageView mIvLock;

	private CircularProgressView mCpvWait;
	private ImageView mCopyLogin;
	private ImageView mCopyPsw;
	private ImageView mShowPsw;
	private TextView mBtnDelete;

	private boolean isPassworded;
	private CollapsingToolbarLayout mCtbMain;

	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_data);
		getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

		Toolbar toolbar = (Toolbar) findViewById(R.id.tb_data_activity);
		setSupportActionBar(toolbar);

		if (getSupportActionBar() != null) {
			getSupportActionBar().setDisplayHomeAsUpEnabled(true);
			getSupportActionBar().setDisplayShowHomeEnabled(true);
		}

		Utilities.setupHideKeyboard(this, findViewById(R.id.cl_data_main));
		Utilities.setupHideKeyboard(this, findViewById(R.id.nested_data));

		mActivity = this;
		isPassworded = true;

		//Init Components
		mCtbMain = (CollapsingToolbarLayout) findViewById(R.id.ctb_data_activity);

		mLblLink = (TextView) findViewById(R.id.lblDataLink);
		mLblLogin = (TextView) findViewById(R.id.lblDataLogin);
		mLblName = (TextView) findViewById(R.id.lblDataName);
		mLblPassword = (TextView) findViewById(R.id.lblDataPassword);

		mTxtLink = (EditText) findViewById(R.id.txtDataLink);
		mTxtLogin = (EditText) findViewById(R.id.txtDataLogin);
		mTxtName = (EditText) findViewById(R.id.txtDataName);
		mTxtPassword = (EditText) findViewById(R.id.txtDataPassword);

		mSpSecure = (Spinner) findViewById(R.id.spDataSecure);
		mIvLock = (ImageView) findViewById(R.id.ivDataLock);
		mBtnProcess = (Button) findViewById(R.id.btnProcessData);
		mBtnDelete = (TextView) findViewById(R.id.btnDataDelete);

		mCopyLogin = (ImageView) findViewById(R.id.ivDataCopy);
		mCopyPsw = (ImageView) findViewById(R.id.ivPsvCopy);
		mShowPsw = (ImageView) findViewById(R.id.ivPsvShow);

		mCopyLogin.setVisibility(View.GONE);
		mCopyPsw.setVisibility(View.GONE);
		mBtnDelete.setVisibility(View.GONE);

		mShowPsw.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				if (isPassworded) {
					mTxtPassword.setTransformationMethod(null);
					isPassworded = false;
				} else {
					mTxtPassword.setTransformationMethod(PasswordTransformationMethod.getInstance());
					isPassworded = true;
				}
			}
		});

		mCpvWait = (CircularProgressView) findViewById(R.id.cpv_data_wait);

		setAvailableUI(true);
		mBtnProcess.setOnClickListener(addNewEntityListener);

		mTxtName.addTextChangedListener(new TextWatcher() {
			@Override public void beforeTextChanged(CharSequence s, int start, int count, int after) {}
			@Override public void onTextChanged(CharSequence s, int start, int before, int count) {}
			@Override public void afterTextChanged(Editable s) {
				mCtbMain.setTitle(s.toString().equals("") ? getString(R.string.title_activity_add_new_data) : s.toString());
			}
		});
	}

	@Override public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
			case android.R.id.home:
				finish();
				break;

			default:
				break;
		}

		return super.onOptionsItemSelected(item);
	}

	public void setAvailableUI(boolean v){
		if (v) {
			mBtnProcess.setVisibility(View.VISIBLE);
			mBtnDelete.setVisibility(View.VISIBLE);
			mCpvWait.setVisibility(View.GONE);
		} else {
			mBtnProcess.setVisibility(View.GONE);
			mBtnDelete.setVisibility(View.GONE);
			mCpvWait.setVisibility(View.VISIBLE);
		}

		mTxtName.setEnabled(v);
		mTxtName.setClickable(v);
		mTxtPassword.setEnabled(v);
		mTxtPassword.setClickable(v);
		mTxtLink.setEnabled(v);
		mTxtLink.setClickable(v);
		mTxtLogin.setEnabled(v);
		mTxtLogin.setClickable(v);
		mBtnProcess.setEnabled(v);
		mBtnProcess.setClickable(v);
 }
	
	 View.OnClickListener backListener = new OnClickListener() {
	 public void onClick(View v) { 
		 mActivity.finish();
		 } 
	 };

	 @Override public boolean onKeyDown(int keyCode, KeyEvent event) {
         if (keyCode == KeyEvent.KEYCODE_BACK && event.getRepeatCount() == 0) {
             mActivity.finish();
             return true;
         }
         return super.onKeyDown(keyCode, event);
     }

   View.OnClickListener addNewEntityListener = new OnClickListener() {
        @Override
		public void onClick(View v) {
			setAvailableUI(false);
			if (mTxtLink.getText().toString().equals("") ||
					mTxtLogin.getText().toString().equals("") ||
					mTxtName.getText().toString().equals("") ||
					mTxtPassword.getText().toString().equals("") || (mSpSecure.getSelectedItemPosition() < 0)) {
                Toasty.warning(getApplicationContext(), getString(R.string.info_all_fields), Toast.LENGTH_SHORT).show();
			}

            SharedPreferences settings = getSharedPreferences("UserData", 0);
            RelationDataModel rDataModel = new RelationDataModel();
            rDataModel.checkinfo = "";
            rDataModel.loginfo = Encryptor.encrypt(settings.getString("salt", "null"),
                    Encryptor.generateIv(settings.getString("email", "null")), mTxtLogin.getText().toString());
            rDataModel.passinfo = Encryptor.encrypt(settings.getString("salt", "null"),
                    Encryptor.generateIv(settings.getString("email", "null")), mTxtPassword.getText().toString());
            rDataModel.securelvl = String.valueOf(mSpSecure.getSelectedItemPosition());
            rDataModel.nameinfo = mTxtName.getText().toString();
            rDataModel.urlinfo = Encryptor.encrypt(settings.getString("salt", "null"),
                    Encryptor.generateIv(settings.getString("email", "null")), mTxtLink.getText().toString());
            rDataModel.typeinfo = getString(R.string.type_data);
            rDataModel.userid = settings.getString("userid", "");

            Pack pack = new Pack(getApplicationContext());
			RetrofitApi client = new RetrofitApi(getApplicationContext());
			RetrofitApi.PostJson apiCall = client.getRetrofitApi().create(RetrofitApi.PostJson.class);
			apiCall.post("tables/AddTable", pack.packing(rDataModel))
					.enqueue(new Callback<JsonObject>() {
						@Override
						public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
							setAvailableUI(true);
							Toasty.success(getApplicationContext(), getString(R.string.data_success), Toast.LENGTH_SHORT).show();
							startActivity(new Intent(getApplicationContext(), LoggedInActivity.class));
						}

						@Override
						public void onFailure(Call<JsonObject> call, Throwable t) {
							setAvailableUI(true);
							Toasty.error(getApplicationContext(), ErrorService.ParseError(t.getLocalizedMessage()), Toast.LENGTH_SHORT).show();
						}
					});
		}
	};
}
