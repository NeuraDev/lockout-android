package com.lockout.keys.activities;

import android.app.ActionBar;
import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.github.rahatarmanahmed.cpv.CircularProgressView;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.lockout.keys.ErrorService;
import com.lockout.keys.Pack;
import com.lockout.keys.R;
import com.lockout.keys.encrpytion.Encryptor;
import com.lockout.keys.helpers.AppData;
import com.lockout.keys.helpers.Utilities;
import com.lockout.keys.http.RetrofitApi;
import com.lockout.keys.models.RelationDataModel;
import com.squareup.okhttp.internal.Util;

import es.dmoral.toasty.Toasty;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

/**
 * Created by sunwi on 05.12.2015.
 */
public class AddNewNoteActivity extends AppCompatActivity {
    private Activity mActivity;
    private TextView mLblName;
    private TextView mLblText;

    private EditText mTxtName;
    private EditText mTxtText;

    private Spinner mSpSecure;
    private ImageView mIvLock;
    private Button mBtnProcess;
    private CircularProgressView mCpvWait;
    private RetrofitApi.PostJson mApiCall;
    private CollapsingToolbarLayout mCtbMain;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_note);

        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

        Toolbar toolbar = (Toolbar) findViewById(R.id.tb_note_activity);
        setSupportActionBar(toolbar);

        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }

        Utilities.setupHideKeyboard(this, findViewById(R.id.cl_note_main));
        Utilities.setupHideKeyboard(this, findViewById(R.id.nested_note));

        mActivity = this;

        Retrofit client = new RetrofitApi(getApplicationContext()).getRetrofitApi();
        mApiCall = client.create(RetrofitApi.PostJson.class);

        //Init Components
        mCtbMain = (CollapsingToolbarLayout) findViewById(R.id.ctb_note_activity);
        mLblName = (TextView) findViewById(R.id.lblNoteName);
        mLblText = (TextView) findViewById(R.id.lblNoteText);

        mTxtName = (EditText) findViewById(R.id.txtNoteName);
        mTxtText = (EditText) findViewById(R.id.txtNoteText);

        mSpSecure = (Spinner) findViewById(R.id.spNoteSecure);
        mIvLock = (ImageView) findViewById(R.id.ivNoteLock);
        mBtnProcess = (Button) findViewById(R.id.btnNoteProcess);

        mCpvWait = (CircularProgressView) findViewById(R.id.cpv_note_wait);

        setAvailableUI(true);
        mBtnProcess.setOnClickListener(processClickListener);

        mTxtName.addTextChangedListener(new TextWatcher() {
            @Override public void beforeTextChanged(CharSequence s, int start, int count, int after) {}
            @Override public void onTextChanged(CharSequence s, int start, int before, int count) {}
            @Override public void afterTextChanged(Editable s) {
                mCtbMain.setTitle(s.toString().equals("") ? getString(R.string.title_activity_add_new_data) : s.toString());
            }
        });
    }

    private void setAvailableUI(boolean v) {
        mTxtName.setEnabled(v);
        mTxtName.setClickable(v);
        mTxtText.setEnabled(v);
        mTxtText.setClickable(v);
        mBtnProcess.setVisibility(v ? View.VISIBLE : View.GONE);
        mCpvWait.setVisibility(v? View.GONE : View.VISIBLE);
    }

    private View.OnClickListener backListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            mActivity.finish();
        }
    };

    @Override public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                break;

            default:
                break;
        }

        return super.onOptionsItemSelected(item);
    }

    View.OnClickListener processClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if (mTxtText.getText().toString().equals("") || mTxtName.getText().toString().equals("")
                    || (mSpSecure.getSelectedItemPosition() < 0)) {
                Toasty.warning(getApplicationContext(), getString(R.string.info_all_fields), Toast.LENGTH_SHORT).show();
            } else {
                setAvailableUI(false);
                RelationDataModel rDataModel = new RelationDataModel();
                rDataModel.checkinfo = "";
                rDataModel.typeinfo = getString(R.string.type_note);
                rDataModel.nameinfo = mTxtName.getText().toString();
                rDataModel.noteinfo = Encryptor.encrypt(AppData.SALT, Encryptor.generateIv(AppData.EMAIL),
                        mTxtText.getText().toString());
                rDataModel.securelvl = String.valueOf(mSpSecure.getSelectedItemPosition());
                rDataModel.userid = AppData.USER_ID;

                mApiCall.post("tables/AddTable", new Pack(getApplicationContext()).packing(rDataModel))
                        .enqueue(new Callback<JsonObject>() {
                            @Override
                            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                                setAvailableUI(true);
                                Toasty.success(getApplicationContext(), getString(R.string.added_note), Toast.LENGTH_SHORT).show();
                                startActivity(new Intent(getApplicationContext(), LoggedInActivity.class));
                            }

                            @Override
                            public void onFailure(Call<JsonObject> call, Throwable t) {
                                setAvailableUI(true);
                                Toasty.error(getApplicationContext(), ErrorService.ParseError(t.getLocalizedMessage()), Toast.LENGTH_SHORT).show();
                            }
                        });
            }
        }
    };
}
