package com.lockout.keys.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.github.rahatarmanahmed.cpv.CircularProgressView;
import com.google.gson.JsonObject;
import com.lockout.keys.Authentication;
import com.lockout.keys.Pack;
import com.lockout.keys.R;
import com.lockout.keys.encrpytion.Encryptor;
import com.lockout.keys.helpers.AppData;
import com.lockout.keys.helpers.Utilities;
import com.lockout.keys.http.RetrofitApi;
import com.lockout.keys.models.Auth1Output;
import com.lockout.keys.models.KeyWord;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import java.util.ArrayList;
import java.util.Date;

import es.dmoral.toasty.Toasty;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

/**
 * Created by agladkov on 04.04.17.
 */

public class ChangePasswordActivity extends AppCompatActivity {
    private final String TAG = "CHANGEPASSWORD";
    private Button mBtnNext;
    private CircularProgressView mCpvWait;
    private ArrayList<String> timeArray;
    private long zeropoint;
    private String currentstring;
    private EditText mTxtCodeword;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_check_user);

        mBtnNext = (Button) findViewById(R.id.btn_check_next);
        mCpvWait = (CircularProgressView) findViewById(R.id.cpv_check_wait);

        Retrofit client = new RetrofitApi(getApplicationContext()).getRetrofitApi();
        final RetrofitApi.PostJson mApiCall = client.create(RetrofitApi.PostJson.class);

        CheckBox mChkAsk = (CheckBox) findViewById(R.id.chk_check_ask);
        final EditText mTxtPassword = (EditText) findViewById(R.id.txt_check_password);
        mTxtCodeword = (EditText) findViewById(R.id.txt_check_codeword);
        TextView mTxtCodewordCaption = (TextView) findViewById(R.id.txt_check_codeword_caption);

        timeArray = new ArrayList<>();
        reinit();

        mTxtCodeword.addTextChangedListener(new TextWatcher() {
            @Override public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {}
            @Override public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                long currenttime = System.currentTimeMillis();
                if (timeArray.size() == 0) {
                    zeropoint = currenttime;
                }

                if (currenttime - zeropoint > 60000) {
                    zeropoint = currenttime;
                    Toast.makeText(getApplicationContext(), "Пожалуйста, введите слово в течение 60 секунд", Toast.LENGTH_SHORT).show();
                    reinit();
                    return;
                }

                timeArray.add(String.valueOf(System.currentTimeMillis() - zeropoint));
            }
            @Override public void afterTextChanged(Editable editable) {
                if (currentstring.length() > mTxtCodeword.getText().length()) {
                    Toast.makeText(getApplicationContext(), "Пожалуйста, введите слово без ошибок", Toast.LENGTH_SHORT).show();
                    reinit();
                    return;
                }

                if (timeArray.size() != 0) {
                    currentstring = mTxtCodeword.getText().toString();
                    timeArray.add(String.valueOf(System.currentTimeMillis() - zeropoint));
                }
            }
        });

        mChkAsk.setVisibility(View.GONE);
        mTxtCodewordCaption.setText(mTxtCodewordCaption.getText().toString() + " - " + AppData.CODEWORD);
        mBtnNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setUIState(false);
                final Auth1Output loginAuth = new Auth1Output();
		        final Pack pack = new Pack(getApplicationContext());
                final Authentication authentication = new Authentication();

                loginAuth.i = AppData.EMAIL;
                authentication.RandomString();

                mApiCall.post("tables/PasswordChange0", pack.packing(loginAuth)).enqueue(new Callback<JsonObject>() {
                    @Override
                    public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                        if (Utilities.validateCode(response.code())) {
                            Auth1Output auth = pack.unpacking(response.body(), Auth1Output.class);
                            Utilities.SRV1Step(loginAuth, auth, authentication);
                            mApiCall.post("tables/PasswordChange1", pack.packing(loginAuth)).enqueue(new Callback<JsonObject>() {
                                @Override
                                public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                                    if (Utilities.validateCode(response.code())) {
                                        final Auth1Output auth = pack.unpacking(response.body(), Auth1Output.class);
                                        Utilities.SRV2Step(loginAuth, auth, AppData.EMAIL,
                                                mTxtPassword.getText().toString(), authentication);

                                        final DateTimeFormatter formatter = DateTimeFormat.forPattern("dd.MM.yyyy HH:mm:ss");
                                        mApiCall.post("tables/PasswordChange2", pack.packing(loginAuth)).enqueue(new Callback<JsonObject>() {
                                            @Override
                                            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                                                if (Utilities.validateCode(response.code())) {
                                                    if (Utilities.SRVCheckR(pack.unpacking(response.body(), Auth1Output.class), authentication)) {
                                                        KeyWord keyWord = KeyWord.createInstance(AppData.LOG_SALT, timeArray,
                                                                Encryptor.encrypt(AppData.LOG_SALT,
                                                                        Encryptor.generateIv(AppData.EMAIL),
                                                                        formatter.print(new DateTime().toLocalDateTime())),
                                                                        AppData.USER_ID,
                                                                        mTxtCodeword.getText().toString());

                                                        if (AppData.SOURCE_TYPE == AppData.SOURCE_DELETE) {
                                                            mApiCall.post("tables/DeleteAccount", pack.packing(keyWord))
                                                                    .enqueue(new Callback<JsonObject>() {
                                                                        @Override
                                                                        public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                                                                            if (Utilities.validateCode(response.code())) {
                                                                                startActivity(new Intent(getApplicationContext(), LoginActivity.class));
                                                                                Toasty.success(getApplicationContext(), getString(R.string.deleted_user), Toast.LENGTH_SHORT).show();
                                                                                setUIState(true);
                                                                            } else {
                                                                                printError("Status code - " + response.code());
                                                                            }
                                                                        }

                                                                        @Override
                                                                        public void onFailure(Call<JsonObject> call, Throwable t) {
                                                                            printError(getString(R.string.error_credentials));
                                                                        }
                                                                    });
                                                        } else {

                                                        }
                                                    } else {
                                                        printError(getString(R.string.error_credentials));
                                                    }
                                                } else {
                                                    printError("Status code - " + response.code());
                                                }
                                            }

                                            @Override
                                            public void onFailure(Call<JsonObject> call, Throwable t) {
                                                printError(t.getLocalizedMessage());
                                            }
                                        });
                                    } else {
                                        printError("Status code - " + response.code());
                                    }
                                }

                                @Override
                                public void onFailure(Call<JsonObject> call, Throwable t) {
                                    printError(t.getLocalizedMessage());
                                }
                            });
                        } else {
                            printError("Status code - " + response.code());
                        }
                    }

                    @Override
                    public void onFailure(Call<JsonObject> call, Throwable t) {
                        printError(t.getLocalizedMessage());
                    }
                });
            }
        });
    }

    private void reinit() {
        currentstring = "";
        mTxtCodeword.setText("");
        timeArray.clear();
        zeropoint = 0;
    }

    public void setUIState(boolean v) {
        mCpvWait.setVisibility(v ? View.GONE : View.VISIBLE);
        mBtnNext.setVisibility(v ? View.VISIBLE : View.GONE);
    }

    public void printError(String error) {
        Toasty.error(getApplicationContext(), error, Toast.LENGTH_SHORT).show();
        setUIState(true);
    }
}
