package com.lockout.keys.activities;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.github.rahatarmanahmed.cpv.CircularProgressView;
import com.google.gson.JsonObject;
import com.lockout.keys.Authentication;
import com.lockout.keys.Pack;
import com.lockout.keys.R;
import com.lockout.keys.encrpytion.Encryptor;
import com.lockout.keys.helpers.AppData;
import com.lockout.keys.helpers.Utilities;
import com.lockout.keys.http.RetrofitApi;
import com.lockout.keys.models.Auth1Output;
import com.lockout.keys.models.KeyWord;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;

import es.dmoral.toasty.Toasty;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

public class CheckUserActivity extends AppCompatActivity {
    private final String TAG = "CHECKUSERACTIVITY";
	private Activity mActivity;

	private ArrayList<String> timeArray;
	private String currentstring;
	private long zeropoint;
	private Button mBtnNext;
	private CircularProgressView mCpvWait;
	private Retrofit mClient;
	private RetrofitApi.PostJson mApiCall;
	private EditText mTxtPassword;
	private EditText mTxtCodeword;
	private TextView mTxtCodewordCaption;
	private CheckBox mChkAsk;
	private Spinner mSpTime;
    private CoordinatorLayout mLayoutMain;
    private Pack mPack;
    private SharedPreferences preferences;

    @Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
		setContentView(R.layout.activity_check_user);

		mActivity = this;
		mClient = new RetrofitApi(getApplicationContext()).getRetrofitApi();
		mApiCall = mClient.create(RetrofitApi.PostJson.class);
        mPack = new Pack(getApplicationContext());

        preferences = getSharedPreferences("UserData", 0);

		Toolbar toolbar = (Toolbar) findViewById(R.id.tb_check_user_activity);
		setSupportActionBar(toolbar);

		if (getSupportActionBar() != null) {
			getSupportActionBar().setDisplayHomeAsUpEnabled(true);
			getSupportActionBar().setDisplayShowHomeEnabled(true);
		}

		Utilities.setupHideKeyboard(this, findViewById(R.id.cl_check_user));
		Utilities.setupHideKeyboard(this, findViewById(R.id.nested_check_user));

        mLayoutMain = (CoordinatorLayout) findViewById(R.id.cl_check_user);
		mCpvWait = (CircularProgressView) findViewById(R.id.cpv_check_wait);
		mTxtPassword = (EditText) findViewById(R.id.txt_check_password);
		mTxtCodeword = (EditText) findViewById(R.id.txt_check_codeword);
		mTxtCodewordCaption = (TextView) findViewById(R.id.txt_check_codeword_caption);
        mBtnNext = (Button) findViewById(R.id.btn_check_next);
		mChkAsk = (CheckBox) findViewById(R.id.chk_check_ask);
		mSpTime = (Spinner) findViewById(R.id.sp_check_time);

        mLayoutMain.setOnClickListener(mainClickListener);
		mChkAsk.setOnCheckedChangeListener(checkedClickListener);
        mBtnNext.setOnClickListener(nextClickListener);

        mLayoutMain.requestFocus();
        mTxtCodeword.setVisibility(Integer.valueOf(AppData.CURRENT_HEADER.getSecurelvl())
                == AppData.SECURITY_MAX ? View.VISIBLE : View.GONE);
        mTxtCodewordCaption.setVisibility(Integer.valueOf(AppData.CURRENT_HEADER.getSecurelvl())
                == AppData.SECURITY_MAX ? View.VISIBLE : View.GONE);

        timeArray = new ArrayList<>();
		reinit();

        mTxtCodewordCaption.setText(mTxtCodewordCaption.getText().toString() + " - " + AppData.CODEWORD);
        mTxtCodeword.addTextChangedListener(new TextWatcher() {
			 @Override public void onTextChanged(CharSequence s, int start, int before, int count) {}
			 @Override public void beforeTextChanged(CharSequence s, int start, int count,
										   int after) {
				 long currenttime = System.currentTimeMillis();
				 if (timeArray.size() == 0) {
					 zeropoint = currenttime;
				 }

				 if (currenttime - zeropoint > 60000) {
					 zeropoint = currenttime;
					 Toast.makeText(getApplicationContext(), "Пожалуйста, введите слово в течение 60 секунд", Toast.LENGTH_SHORT).show();
					 reinit();
					 return;
				 }

				 timeArray.add(String.valueOf(System.currentTimeMillis() - zeropoint));
			 }

			 @Override public void afterTextChanged(Editable s) {
				 if (currentstring.length() > mTxtCodeword.getText().length()) {
					 Toast.makeText(getApplicationContext(), "Пожалуйста, введите слово без ошибок", Toast.LENGTH_SHORT).show();
					 reinit();
					 return;
				 }

				 if (timeArray.size() != 0) {
					 currentstring = mTxtCodeword.getText().toString();
					 timeArray.add(String.valueOf(System.currentTimeMillis() - zeropoint));
				 }
			 }
		 });
//
//		mTxtCheckCodeword.setOnEditorActionListener(new TextView.OnEditorActionListener() {
//			@Override
//			public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
//				boolean handled = false;
//				if (actionId == EditorInfo.IME_ACTION_GO) {
//                  /* Write your logic here that will be executed when user taps next button */
//					mBtnNext.performClick();
//					handled = true;
//				}
//				return handled;
//			}
//		});
	}

	@Override public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
			case android.R.id.home:
				finish();
				break;

			default:
				break;
		}

		return super.onOptionsItemSelected(item);
	}

	private CompoundButton.OnCheckedChangeListener checkedClickListener = new CompoundButton.OnCheckedChangeListener() {
		@Override
		public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
			mSpTime.setVisibility(b ? View.VISIBLE : View.GONE);
		}
	};

	private OnClickListener mainClickListener = new OnClickListener() {
        @Override
        public void onClick(View view) {
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
            mLayoutMain.requestFocus();
        }
    };

	private void reinit() {
		currentstring = "";
		mTxtCodeword.setText("");
		timeArray.clear();
		zeropoint = 0;
	}
	
	public ArrayList<String> cutArray(ArrayList<String> input, int count) {
		SharedPreferences settings = getSharedPreferences("UserData", 0);
		ArrayList<String> result = new ArrayList<>();
		for (int i = 0; i < input.size(); i++) {
			if (input.get(i).equalsIgnoreCase("")) {
				result.add("");
			} else {
				result.add(Encryptor.encrypt(settings.getString("newsalt", "null"), Encryptor.generateIv(settings.getString("newemail", "null")), input.get(i)));
				Log.e("data", "newsalt - " + settings.getString("newsalt", "null") + " newemail - " + Encryptor.generateIv(settings.getString("newemail", "null")));
			}
		}
		return result;
	}

	public ArrayList<String> decodeArray(ArrayList<String> input) {
		SharedPreferences settings = getSharedPreferences("UserData", 0);
		ArrayList<String> result = new ArrayList<String>();
		for (int i = 0; i < input.size(); i++) {
			if (input.get(i).equalsIgnoreCase("") || input.get(i) == null) {
				result.add("");
			} else {
				result.add(Encryptor.decrypt(settings.getString("salt", "null"), Encryptor.generateIv(settings.getString("email", "null")), input.get(i)));
			}
		}
		return result;
	}

	public List<String> encodeArray(ArrayList<String> input) {
		SharedPreferences settings = getSharedPreferences("UserData", 0);
		ArrayList<String> result = new ArrayList<String>();
		for (int i = 0; i < input.size(); i++) {
			if (input.get(i).equalsIgnoreCase("") || input.get(i) == null) {
				result.add("");
			} else {
				result.add(Encryptor.encrypt(settings.getString("newsalt", null), Encryptor.generateIv(settings.getString("newemail", null)), input.get(i)));
			}
		}
		return result;
	}

	public void ChangePSW() {
//		setAvailableUI(AppData.UI_LOADING);
//		final SharedPreferences settings = getSharedPreferences("UserData", 0);
//
//
//
//		SendObject sendObject = pack.packing(loginAuth);
//		final ListenableFuture<SendObject> respond = mAuthService.getClient().invokeApi("tables/PasswordChange0", sendObject, "POST", null, SendObject.class);
//		Futures.addCallback(respond, new FutureCallback<SendObject>() {
//			@Override
//			public void onSuccess(SendObject result) {
//
//
//				SendObject sendObject1 = pack.packing(loginAuth);
//				ListenableFuture<SendObject> respond1 = mAuthService.getClient().invokeApi("tables/PasswordChange1", sendObject1, "POST", null, SendObject.class);
//				Futures.addCallback(respond1, new FutureCallback<SendObject>() {
//					@Override
//					public void onSuccess(SendObject result1) {
//
//
//						SendObject sendObject2 = pack.packing(loginAuth);
//						ListenableFuture<SendObject> respond2 = mAuthService.getClient().invokeApi("tables/PasswordChange2", sendObject2, "POST", null, SendObject.class);
//						Futures.addCallback(respond2, new FutureCallback<SendObject>() {
//							@Override
//							public void onSuccess(SendObject result2) {

//								if (myR.equals(serverR)) {
//									KeyWord keyWord = new KeyWord();
////									keyWord.data = Encryptor.encrypt(settings.getString("logsalt", ""), Encryptor.generateIv(settings.getString("email", "")), timeArray.toString());
////									keyWord.deviceid = Encryptor.encrypt(settings.getString("logsalt", ""), Encryptor.generateIv(settings.getString("email", "")), settings.getString("deviceid", null));
////									keyWord.userid = settings.getString("userid", null);
////									keyWord.keyword = Encryptor.encrypt(settings.getString("logsalt", ""), Encryptor.generateIv(settings.getString("email", "")), mTxtCheckCodeword.getText().toString());
////									SimpleDateFormat formatter = new SimpleDateFormat("dd.MM.yyyy HH:mm:ss");
////									keyWord.date = ;
//
//									SendObject sendObject3 = pack.packing(keyWord);
//									if (getSharedPreferences("UserData", 0).getString("source", "").equalsIgnoreCase("delete")) {
//										ListenableFuture<SendObject> respond3 = mAuthService.getClient().invokeApi("tables/DeleteAccount", sendObject3, "POST", null, SendObject.class);
//										Futures.addCallback(respond3, new FutureCallback<SendObject>() {
//
//											@Override
//											public void onSuccess(SendObject result) {
//												Intent backIntent = new Intent(getApplicationContext(), LoginActivity.class);
//												startActivity(backIntent);
//												Toast.makeText(getApplicationContext(), "До свидания! Нам будет вас не хватать :(", Toast.LENGTH_SHORT).show();
//												setAvailableUI(AppData.UI_VISIBLE);
//											}
//
//											@Override
//											public void onFailure(Throwable t) {
//												Intent backIntent = new Intent(getApplicationContext(), SettingsActivity.class);
//												startActivity(backIntent);
//												Toast.makeText(getApplicationContext(), ErrorService.ParseError(t.getLocalizedMessage()), Toast.LENGTH_SHORT).show();
//												setAvailableUI(AppData.UI_VISIBLE);
//											}
//										});
//									} else {
//										ListenableFuture<SendObject> respond3 = mAuthService.getClient().invokeApi("tables/PasswordChange3", sendObject3, "POST", null, SendObject.class);
//										Futures.addCallback(respond3, new FutureCallback<SendObject>() {
//											@Override
//											public void onSuccess(SendObject result3) {
//												HeadersListModel hListModel = pack.unpacking(result3, HeadersListModel.class);
//												ArrayList<String> SecureArray = new ArrayList<String>();
//												ArrayList<String> UrlArray = new ArrayList<String>();
//												ArrayList<String> LogArray = new ArrayList<String>();
//												ArrayList<String> PassArray = new ArrayList<String>();
//												ArrayList<String> NoteArray = new ArrayList<String>();
//												ArrayList<String> IdArray = new ArrayList<String>();
//												ArrayList<String> TypeArray = new ArrayList<String>();
//												for (int i = 0; i < hListModel.headers.size(); i++) {
//													SecureArray.add(hListModel.data.get(i).securelvl);
//													if (hListModel.data.get(i).urlinfo == null) {
//														UrlArray.add("");
//													} else {
//														UrlArray.add(hListModel.data.get(i).urlinfo);
//													}
//													if (hListModel.data.get(i).passinfo == null) {
//														PassArray.add("");
//													} else {
//														PassArray.add(hListModel.data.get(i).passinfo);
//													}
//													if (hListModel.data.get(i).loginfo == null) {
//														LogArray.add("");
//													} else {
//														LogArray.add(hListModel.data.get(i).loginfo);
//													}
//													if (hListModel.data.get(i).noteinfo == null) {
//														NoteArray.add("");
//													} else {
//														NoteArray.add(hListModel.data.get(i).noteinfo);
//													}
//													IdArray.add(hListModel.data.get(i).dataid);
//													TypeArray.add(hListModel.data.get(i).typeinfo);
//												}
//
//												if (!settings.getString("newpassword", "*******").equalsIgnoreCase("*******"))  {
//													try {
//														String newsalt = Encryptor.getPBKDF2(settings.getString("userid", ""), settings.getString("newpassword", ""));
//														settings.edit().putString("newsalt", newsalt).commit();
//														UrlArray = (ArrayList<String>) encodeArray(decodeArray(UrlArray));
//														LogArray = (ArrayList<String>) encodeArray(decodeArray(LogArray));
//														PassArray = (ArrayList<String>) encodeArray(decodeArray(PassArray));
//														NoteArray = (ArrayList<String>) encodeArray(decodeArray(NoteArray));
//													} catch (Exception e) {
//														Log.e("Error", ErrorService.ParseError(e.getLocalizedMessage()));
//													}
//												} else {
//													try {
//														String newsalt = Encryptor.getPBKDF2(settings.getString("userid", ""), settings.getString("password", ""));
//														settings.edit().putString("newsalt", newsalt).commit();
//														UrlArray = (ArrayList<String>) encodeArray(decodeArray(UrlArray));
//														LogArray = (ArrayList<String>) encodeArray(decodeArray(LogArray));
//														PassArray = (ArrayList<String>) encodeArray(decodeArray(PassArray));
//														NoteArray = (ArrayList<String>) encodeArray(decodeArray(NoteArray));
//													} catch (Exception e) {
//														Log.e("Error", ErrorService.ParseError(e.getLocalizedMessage()));
//													}
//												}
//
//												ArrayList<RelationDataModel> rDataModels = new ArrayList<RelationDataModel>();
//												for (int i = 0; i < IdArray.size(); i++) {
//													RelationDataModel rDataModel = new RelationDataModel();
//													rDataModel.dataid = IdArray.get(i);
//													rDataModel.loginfo = LogArray.get(i);
//													rDataModel.passinfo = PassArray.get(i);
//													rDataModel.noteinfo = NoteArray.get(i);
//													rDataModel.urlinfo = UrlArray.get(i);
//													rDataModel.typeinfo = TypeArray.get(i);
//													rDataModel.userid = settings.getString("userid", "");
//													rDataModels.add(rDataModel);
//												}
//
//												RegistrationTool registrationTool = new RegistrationTool();
//												if (!settings.getString("newpassword", "*******").equalsIgnoreCase("*******"))  {
//													registrationTool.getX(settings.getString("newpassword", ""));
//												} else {
//													registrationTool.getX(settings.getString("password", ""));
//												}
//
//												registrationTool.getN(settings.getString("newemail", ""));
//												ChangePasswordModel cPasswordModel = new ChangePasswordModel();
//												cPasswordModel.data = rDataModels;
//												cPasswordModel.email = settings.getString("newemail", "");
//												cPasswordModel.salt = registrationTool.s;
//												cPasswordModel.password = registrationTool.getV().toString();
//												cPasswordModel.id = settings.getString("userid", "");
//
//												Pack pack2 = new Pack(getApplicationContext());
//												SendObject sendObject4 = pack2.packing(cPasswordModel);
//												ListenableFuture<SendObject> respond4 = mAuthService.getClient().invokeApi("tables/PasswordChange4", sendObject4, "POST", null, SendObject.class);
//												Futures.addCallback(respond4, new FutureCallback<SendObject>() {
//													@Override
//													public void onSuccess(SendObject result) {
//														Toast.makeText(getApplicationContext(), "Ваши данные успешно обновлены", Toast.LENGTH_SHORT).show();
//														Intent errorIntent = new Intent(getApplicationContext(), LoginActivity.class);
//														startActivity(errorIntent);
//														setAvailableUI(AppData.UI_VISIBLE);
//													}
//
//													@Override
//													public void onFailure(Throwable t) {
//														Toast.makeText(getApplicationContext(), ErrorService.ParseError(t.getLocalizedMessage()), Toast.LENGTH_SHORT).show();
//														Intent errorIntent = new Intent(getApplicationContext(), LoginActivity.class);
//														startActivity(errorIntent);
//														setAvailableUI(AppData.UI_VISIBLE);
//													}
//												});
//											}
//
//											@Override
//											public void onFailure(Throwable t) {
//												Intent backIntent = new Intent(getApplicationContext(), SettingsActivity.class);
//												startActivity(backIntent);
//												Toast.makeText(getApplicationContext(), ErrorService.ParseError(t.getLocalizedMessage()), Toast.LENGTH_SHORT).show();
//												setAvailableUI(AppData.UI_VISIBLE);
//											}
//										});
//									}
//								} else {
//									Intent backIntent = new Intent(getApplicationContext(), SettingsActivity.class);
//									startActivity(backIntent);
//									Toast.makeText(getApplicationContext(), "Пароль или кодовое слово введены неверно", Toast.LENGTH_SHORT).show();
//									setAvailableUI(AppData.UI_VISIBLE);
//								}
//							}
//
//							@Override
//							public void onFailure(Throwable t) {
//								Intent backIntent = new Intent(getApplicationContext(), SettingsActivity.class);
//								startActivity(backIntent);
//								Toast.makeText(getApplicationContext(), ErrorService.ParseError(t.getLocalizedMessage()), Toast.LENGTH_SHORT).show();
//								setAvailableUI(AppData.UI_VISIBLE);
//							}
//						});
//					}
//
//					@Override
//					public void onFailure(Throwable t) {
//						Intent backIntent = new Intent(getApplicationContext(), SettingsActivity.class);
//						startActivity(backIntent);
//						Toast.makeText(getApplicationContext(), ErrorService.ParseError(t.getLocalizedMessage()), Toast.LENGTH_SHORT).show();
//						setAvailableUI(AppData.UI_VISIBLE);
//					}
//				});
//			}
//
//			@Override
//			public void onFailure(Throwable t) {
//				Intent backIntent = new Intent(getApplicationContext(), SettingsActivity.class);
//				startActivity(backIntent);
//				Toast.makeText(getApplicationContext(), ErrorService.ParseError(t.getLocalizedMessage()), Toast.LENGTH_SHORT).show();
//				setAvailableUI(AppData.UI_VISIBLE);
//			}
//		});
	}

	public void showData() {
        if (AppData.CURRENT_HEADER.getTypeinfo().equalsIgnoreCase(getString(R.string.type_note))) {
            startActivity(new Intent(getApplicationContext(), ShowUserNotesActivity.class));
            setAvailableUI(AppData.UI_VISIBLE);
        } else if (AppData.CURRENT_HEADER.getTypeinfo().equalsIgnoreCase(getString(R.string.type_data))){
            startActivity(new Intent(getApplicationContext(), ShowUserDataActivity.class));
            setAvailableUI(AppData.UI_VISIBLE);
        } else {
            startActivity(new Intent(getApplicationContext(), LoggedInActivity.class));
            setAvailableUI(AppData.UI_VISIBLE);
            Toasty.error(getApplicationContext(), getString(R.string.error_inner), Toast.LENGTH_SHORT).show();
        }
        finish();
    }


	View.OnClickListener nextClickListener = new OnClickListener() {
		
		@Override
		public void onClick(View v) {
            if (mTxtPassword.getText().toString().equals("")) {
                Toasty.warning(getApplicationContext(), getString(R.string.info_all_fields), Toast.LENGTH_SHORT).show();
                return;
            }

            if (mTxtCodeword.getVisibility() != View.GONE && mTxtCodeword.getText().toString().equals("")) {
                Toasty.warning(getApplicationContext(), getString(R.string.info_all_fields), Toast.LENGTH_SHORT).show();
                return;
            }

            setAvailableUI(AppData.UI_LOADING);
            final Auth1Output loginAuth = new Auth1Output();
            final Authentication authentication = new Authentication();

            loginAuth.dataid = AppData.CURRENT_HEADER.getDataid();
			loginAuth.i = AppData.EMAIL;
            authentication.RandomString();

            mApiCall.post("tables/SetPermission0", mPack.packing(loginAuth))
                    .enqueue(new Callback<JsonObject>() {
                        @Override
                        public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                            Auth1Output auth = mPack.unpacking(response.body(), Auth1Output.class);

                            authentication.getN(AppData.EMAIL, auth.s);
					        loginAuth.a = authentication.getA().toString();

                            mApiCall.post("tables/SetPermission1", mPack.packing(loginAuth))
                                    .enqueue(new Callback<JsonObject>() {
                                        @Override
                                        public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                                            Auth1Output auth1 = mPack.unpacking(response.body(), Auth1Output.class);

                                            authentication.getU(auth1.b);
                                            String x = authentication.getX(mTxtPassword.getText().toString(), auth1.s).toString();
                                            authentication.getk();
                                            authentication.getS(auth1.b, authentication.g.toString(), x);
                                            authentication.getK();
                                            loginAuth.b = auth1.b;

                                            authentication.getV(x);
                                            loginAuth.id_device = AppData.DEVICE_ID;
                                            loginAuth.m = authentication.getM(authentication.N, authentication.g,
                                                    AppData.EMAIL, auth1.s, authentication.A, auth1.b, authentication.K).toString();

                                            loginAuth.date = mChkAsk.isChecked() ? Utilities
                                                    .convertTime(mSpTime.getSelectedItem().toString()) : "";

                                            mApiCall.post("tables/SetPermissionAdvancedlvl", mPack.packing(loginAuth))
                                                    .enqueue(new Callback<JsonObject>() {
                                                        @Override
                                                        public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                                                            Auth1Output loginAuth = mPack.unpacking(response.body(), Auth1Output.class);
                                                            BigInteger myR = authentication.getR(authentication.A, authentication.M, authentication.K);
                                                            BigInteger serverR = new BigInteger(loginAuth.r);

                                                            String date = mChkAsk.isChecked() ? Utilities
                                                                    .convertTime(mSpTime.getSelectedItem().toString()) : "";

                                                            if (myR.equals(serverR)) {
                                                                if (Integer.valueOf(AppData.CURRENT_HEADER.getSecurelvl()) == AppData.SECURITY_MAX) {
                                                                    KeyWord keyWord = KeyWord.createInstance(
                                                                            AppData.LOG_SALT, timeArray,
                                                                            date, AppData.USER_ID,
																			mTxtCodeword.getText().toString());

                                                                    mApiCall.post("tables/SetPermissionSecurelvl", mPack.packing(keyWord))
                                                                            .enqueue(new Callback<JsonObject>() {
                                                                                @Override
                                                                                public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                                                                                    showData();
                                                                                }

                                                                                @Override
                                                                                public void onFailure(Call<JsonObject> call, Throwable t) {
                                                                                    setAvailableUI(AppData.UI_VISIBLE);
                                                                                    Toasty.error(getApplicationContext(), getString(R.string.error_check), Toast.LENGTH_SHORT).show();
                                                                                }
                                                                            });
                                                                } else {
                                                                    showData();
                                                                }
                                                            } else {
                                                                setAvailableUI(AppData.UI_VISIBLE);
                                                                Toasty.error(getApplicationContext(), getString(R.string.error_check), Toast.LENGTH_SHORT).show();
                                                            }
                                                        }

                                                        @Override
                                                        public void onFailure(Call<JsonObject> call, Throwable t) {
                                                            Toasty.error(getApplicationContext(), t.getLocalizedMessage(), Toast.LENGTH_SHORT).show();
                                                            setAvailableUI(AppData.UI_VISIBLE);
                                                        }
                                                    });
                                        }

                                        @Override
                                        public void onFailure(Call<JsonObject> call, Throwable t) {
                                            Toasty.error(getApplicationContext(), t.getLocalizedMessage(), Toast.LENGTH_SHORT).show();
                                            setAvailableUI(AppData.UI_VISIBLE);
                                        }
                                    });
                        }

                        @Override
                        public void onFailure(Call<JsonObject> call, Throwable t) {
                            Toasty.error(getApplicationContext(), t.getLocalizedMessage(), Toast.LENGTH_SHORT).show();
                            setAvailableUI(AppData.UI_VISIBLE);
                        }
                    });
		}
	};
	
	 public void setAvailableUI(int status) {
         mBtnNext.setVisibility(status == AppData.UI_VISIBLE ? View.VISIBLE : View.GONE);
         mCpvWait.setVisibility(status == AppData.UI_LOADING ? View.VISIBLE : View.GONE);
	 }
}
