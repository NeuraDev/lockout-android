package com.lockout.keys.activities;

import android.app.ActionBar;
import android.app.Activity;
import android.app.ListActivity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.lockout.keys.EmailActivity;
import com.lockout.keys.GridActivity;
import com.lockout.keys.MyArrayAdapter;
import com.lockout.keys.R;
import com.lockout.keys.SMSActivity;
import com.lockout.keys.activities.LoginActivity;
import com.lockout.keys.adapters.RestoreAdapter;
import com.lockout.keys.helpers.ListConfig;
import com.lockout.keys.models.Model;
import com.lockout.keys.models.Restore;

import java.util.ArrayList;

import es.dmoral.toasty.Toasty;

/**
 * Created by sunwi on 08.04.2016.
 */
public class ForgotActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_forgot);

        Toolbar toolbar = (Toolbar) findViewById(R.id.tb_restore_activity);
        setSupportActionBar(toolbar);

        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }

        RecyclerView mRvRestore =  (RecyclerView) findViewById(R.id.recycler_restore);
        ArrayList<Restore> mRestoreList = new ArrayList<>();
        mRestoreList.add(new Restore(0, getString(R.string.restore_type_sms),
                getString(R.string.restore_type_sms_subtitle)));
        mRestoreList.add(new Restore(1, getString(R.string.restore_type_email),
                getString(R.string.restore_type_email_subtitle)));
        mRestoreList.add(new Restore(2, getString(R.string.restore_type_auth),
                getString(R.string.restore_type_auth_subtitle)));
        mRestoreList.add(new Restore(3, getString(R.string.restore_type_table),
                getString(R.string.restore_type_table_subtitle)));
        RestoreAdapter mAdapter = new RestoreAdapter(mRestoreList, itemClickListener);

        new ListConfig.Builder(mAdapter)
                .setHasFixedSize(true)
                .setHasNestedScroll(false)
                .setDefaultDividerEnabled(false)
                .build(getApplicationContext())
                .applyConfig(getApplicationContext(), mRvRestore);
    }

    @Override public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                break;

            default:
                break;
        }

        return super.onOptionsItemSelected(item);
    }

    private RestoreAdapter.OnItemClickListener itemClickListener = new RestoreAdapter.OnItemClickListener() {
        @Override
        public void onItemClick(Restore item) {
            switch (item.getId()) {
            case 0:
                startActivity(new Intent(getApplicationContext(), SMSActivity.class));
                break;
            case 1:
                startActivity(new Intent(getApplicationContext(), EmailActivity.class));
                break;
            case 2:
                Toasty.normal(getApplicationContext(), getString(R.string.restore_authenticator), Toast.LENGTH_LONG).show();
                break;
            case 3:
                startActivity(new Intent(getApplicationContext(), GridActivity.class));
                break;
            default:
                break;
            }
        }
    };
}
