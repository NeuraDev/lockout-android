package com.lockout.keys.activities;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.github.rahatarmanahmed.cpv.CircularProgressView;
import com.google.gson.JsonObject;
import com.lockout.keys.ErrorService;
import com.lockout.keys.Pack;
import com.lockout.keys.R;
import com.lockout.keys.SettingsActivity;
import com.lockout.keys.adapters.HeaderAdapter;
import com.lockout.keys.helpers.AppData;
import com.lockout.keys.helpers.ListConfig;
import com.lockout.keys.http.RetrofitApi;
import com.lockout.keys.models.HeadersListModel;
import com.lockout.keys.models.TypeConnection;

import org.joda.time.DateTime;
import org.joda.time.LocalDate;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import java.util.ArrayList;

import es.dmoral.toasty.Toasty;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoggedInActivity extends AppCompatActivity {
    private final String TAG = "LOGGEDINACTIVITY";
	private Activity mActivity;
	private TextView mLblUsername;
	private TextView mLblExit;
	private Button mBtnAddEntity;
	private CircularProgressView mCpvWait;
	private RecyclerView mRvRecords;

	private TextView mLblProfile;
    private HeaderAdapter mAdapter;
	private Pack pack;
	private ArrayList<TypeConnection> mRecordList;
    private LinearLayout mLayoutMain;
    private TextView mTxtNoItems;

    @Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
		setContentView(R.layout.activity_logged_in);

		mActivity = this;
        mLayoutMain = (LinearLayout) findViewById(R.id.ll_logged_main);
		mLblExit = (TextView) findViewById(R.id.lblLoggedInExit);
		mLblUsername = (TextView) findViewById(R.id.lblLoggedInUsername);
		mBtnAddEntity = (Button) findViewById(R.id.btnLoggedInNew);
		mCpvWait = (CircularProgressView) findViewById(R.id.cpv_logged_wait);
		mRvRecords = (RecyclerView) findViewById(R.id.recycler_logged);
		mLblProfile = (TextView) findViewById(R.id.lblLoggedInProfile);
		pack = new Pack(getApplicationContext());
		mRecordList = new ArrayList<>();
        mAdapter = new HeaderAdapter(mRecordList, itemClickListener);
        mTxtNoItems = (TextView) findViewById(R.id.txt_logged_no_items);

        EditText mTxtSearch = (EditText) findViewById(R.id.txtLoggedInSearch);
        new ListConfig.Builder(mAdapter)
                .setHasFixedSize(true)
                .setHasNestedScroll(true)
                .setDefaultDividerEnabled(false)
                .build(getApplicationContext())
                .applyConfig(getApplicationContext(), mRvRecords);

        mLayoutMain.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
                mLayoutMain.requestFocus();
                return false;
            }
        });
        mLayoutMain.requestFocus();

		mTxtSearch.addTextChangedListener(new TextWatcher() {
			@Override
			public void beforeTextChanged(CharSequence s, int start, int count, int after) {

			}

			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
				/*if (IdArray.size() > 0) {
					adapter.getFilter().filter(s);
				}*/
			}

			@Override
			public void afterTextChanged(Editable s) {

			}
		});
		 
		// Set click listeners
		mLblExit.setOnClickListener(logoutListener);
		mBtnAddEntity.setOnClickListener(newListener);
		mLblProfile.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent profileIntent = new Intent(getApplicationContext(), SettingsActivity.class);
				startActivity(profileIntent);
			}
		});

		loadData();
	}

	private HeaderAdapter.OnItemClickListener itemClickListener = new HeaderAdapter.OnItemClickListener() {
        @Override
        public void onItemClick(TypeConnection item) {
            AppData.CURRENT_HEADER = item;
            boolean Allow = false;

            if (!item.getCheckinfo().equals("")) {
                DateTimeFormatter formatter = DateTimeFormat.forPattern("dd.MM.yyyy HH:mm:ss");
                LocalDate today = new DateTime().toLocalDate();
                LocalDate server = formatter.parseDateTime(item.getCheckinfo()).toLocalDate();

                Allow = today.isBefore(server);
            }

            if (Integer.valueOf(item.getSecurelvl()) < 1 || Allow) {
                if (item.getTypeinfo().equalsIgnoreCase(getString(R.string.type_note))) {
                    startActivity(new Intent(getApplicationContext(), ShowUserNotesActivity.class));
                } else {
                    startActivity(new Intent(getApplicationContext(), ShowUserDataActivity.class));
                }
            } else {
                startActivity(new Intent(getApplicationContext(), CheckUserActivity.class));
            }
        }
    };

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event)
	{
		if (keyCode == KeyEvent.KEYCODE_BACK && event.getRepeatCount() == 0)
		{
			//mAuthService.logout(true);
			return true;
		}
		return super.onKeyDown(keyCode, event);		
	}
	
	public void loadData() {
		setAvailableUI(AppData.UI_LOADING)
        ;
		final HeadersListModel hListModel = new HeadersListModel();
		SharedPreferences preferences = getSharedPreferences("UserData", 0);
		hListModel.deviceid = preferences.getString("deviceid", "");

		mLblUsername.setText(preferences.getString("username", ""));

		RetrofitApi client = new RetrofitApi(getApplicationContext());
		RetrofitApi.PostJson apiCall = client.getRetrofitApi().create(RetrofitApi.PostJson.class);

		apiCall.post("tables/ReadHeaders", pack.packing(hListModel))
				.enqueue(new Callback<JsonObject>() {
					@Override
					public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
						HeadersListModel model = pack.unpacking(response.body(), HeadersListModel.class);

                        mRecordList.clear();
                        mAdapter.notifyDataSetChanged();
						for (int i = 0; i < model.headers.size(); i++) {
							TypeConnection header = model.headers.get(i);
							mRecordList.add(header);
						}

						mAdapter.notifyItemRangeChanged(0, mRecordList.size());
						setAvailableUI(AppData.UI_VISIBLE);
					}

					@Override
					public void onFailure(Call<JsonObject> call, Throwable t) {
						setAvailableUI(AppData.UI_NO_ITEMS);
						Toasty.error(getApplicationContext(), ErrorService.ParseError(t.getLocalizedMessage()), Toast.LENGTH_SHORT).show();
						finishAffinity();
					}
				});
	}
	

	
//	@Override
//	public void onCreateContextMenu(final ContextMenu menu, View v, ContextMenuInfo menuInfo){
//		AdapterView.AdapterContextMenuInfo aMenuInfo = (AdapterView.AdapterContextMenuInfo) menuInfo;
//
//		int menuPosition = 0;
//		Model model = (Model) adapter.getItem(aMenuInfo.position);
//		for (int i = 0; i < IdArray.size(); i++) {
//			if (model.getName().equalsIgnoreCase(NameArray.get(i))) {
//				menuPosition = i;
//			}
//		}
//
//		final SharedPreferences settings = getSharedPreferences("UserData", 0);
//		menu.setHeaderTitle("Меню");
//
//		boolean Allow = false;
//
//		if (ChkStatusArray.get(menuPosition) != "") {
//			SimpleDateFormat sdf = new SimpleDateFormat("dd.MM.yyyy HH:mm:ss");
//			SimpleDateFormat df = new SimpleDateFormat("dd.MM.yyyy HH:mm:ss");
//			df.setTimeZone(TimeZone.getTimeZone("UTC"));
//			String currentStrDateandTime = df.format(new Date());
//			String serverStrDateandTime = ChkStatusArray.get(menuPosition);
//
//			try {
//				Date currentDateandTime = sdf.parse(currentStrDateandTime);
//				Date serverDateandTime = sdf.parse(serverStrDateandTime);
//				if (currentDateandTime.getTime() <= serverDateandTime.getTime()) {
//					Allow = true;
//				}
//			} catch (ParseException e) {
//				e.printStackTrace();
//			}
//		}
//
//		if (Integer.valueOf(SecureArray.get(menuPosition).toString()) == 0 || Allow){
//			final HeadersListModel hListModel = new HeadersListModel();
//			final Pack pack = new Pack(getApplicationContext());
//			hListModel.deviceid = settings.getString("deviceid", null);
//			hListModel.dataid = IdArray.get(menuPosition);
//
//		if (TypeArray.get(menuPosition).equalsIgnoreCase("Данные")) {
//			menu.add("Скопировать логин").setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
//				@Override
//				public boolean onMenuItemClick(MenuItem item) {
//					setAvailableUI(false);
//					SendObject sendObject = pack.packing(hListModel);
//					final ListenableFuture<SendObject> respond = mAuthService.getClient().invokeApi("tables/ReadData", sendObject, "POST", null, SendObject.class);
//					Futures.addCallback(respond, new FutureCallback<SendObject>() {
//						@Override
//						public void onSuccess(SendObject result) {
//							setAvailableUI(true);
//							DataTable dTable = pack.unpacking(result, DataTable.class);
//							ClipboardManager clipboard = (ClipboardManager) getSystemService(CLIPBOARD_SERVICE);
//							clipboard.setText(Encryptor.decrypt(settings.getString("salt", "null"), Encryptor.generateIv(settings.getString("email", "null")), dTable.loginfo));
//							Toast.makeText(getApplicationContext(), "Данные скопированы", Toast.LENGTH_SHORT).show();
//						}
//
//						@Override
//						public void onFailure(Throwable t) {
//							setAvailableUI(true);
//							Toast.makeText(getApplicationContext(), ErrorService.ParseError(t.getLocalizedMessage()), Toast.LENGTH_SHORT).show();
//						}
//					});
//
//					return true;
//				}
//			});
//
//			menu.add("Скопировать пароль").setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
//				@Override
//				public boolean onMenuItemClick(MenuItem item) {
//					setAvailableUI(false);
//					SendObject sendObject = pack.packing(hListModel);
//					final ListenableFuture<SendObject> respond = mAuthService.getClient().invokeApi("tables/ReadData", sendObject, "POST", null, SendObject.class);
//					Futures.addCallback(respond, new FutureCallback<SendObject>() {
//						@Override
//						public void onSuccess(SendObject result) {
//							setAvailableUI(true);
//							DataTable dTable = pack.unpacking(result, DataTable.class);
//							ClipboardManager clipboard = (ClipboardManager) getSystemService(CLIPBOARD_SERVICE);
//							clipboard.setText(Encryptor.decrypt(settings.getString("salt", "null"), Encryptor.generateIv(settings.getString("email", "null")), dTable.passinfo));
//							Toast.makeText(getApplicationContext(), "Данные скопированы", Toast.LENGTH_SHORT).show();
//						}
//
//						@Override
//						public void onFailure(Throwable t) {
//							setAvailableUI(true);
//							Toast.makeText(getApplicationContext(), ErrorService.ParseError(t.getLocalizedMessage()), Toast.LENGTH_SHORT).show();
//						}
//					});
//					return true;
//				}
//			});
//		} else {
//			menu.add("Скопировать текст").setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
//				@Override
//				public boolean onMenuItemClick(MenuItem item) {
//					setAvailableUI(false);
//					SendObject sendObject = pack.packing(hListModel);
//					final ListenableFuture<SendObject> respond = mAuthService.getClient().invokeApi("tables/ReadNote", sendObject, "POST", null, SendObject.class);
//					Futures.addCallback(respond, new FutureCallback<SendObject>() {
//						@Override
//						public void onSuccess(SendObject result) {
//							setAvailableUI(true);
//							NoteTable nTable = pack.unpacking(result, NoteTable.class);
//							ClipboardManager clipboard = (ClipboardManager) getSystemService(CLIPBOARD_SERVICE);
//							clipboard.setText(Encryptor.decrypt(settings.getString("salt", "null"), Encryptor.generateIv(settings.getString("email", "null")), nTable.noteinfo));
//							Toast.makeText(getApplicationContext(), "Данные скопированы", Toast.LENGTH_SHORT).show();
//						}
//
//						@Override
//						public void onFailure(Throwable t) {
//							setAvailableUI(true);
//							Toast.makeText(getApplicationContext(), ErrorService.ParseError(t.getLocalizedMessage()), Toast.LENGTH_SHORT).show();
//						}
//					});
//					return true;
//				}
//			});
//		}
//			final RelationDataModel rDataModel = new RelationDataModel();
//			rDataModel.dataid = IdArray.get(menuPosition);
//			rDataModel.typeinfo = TypeArray.get(menuPosition);
//
//			menu.add("Удалить запись").setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
//				@Override
//				public boolean onMenuItemClick(MenuItem item) {
//					SendObject sendObject = pack.packing(rDataModel);
//					final ListenableFuture<SendObject> respond = mAuthService.getClient().invokeApi("tables/Delete", sendObject, "POST", null, SendObject.class);
//					setAvailableUI(false);
//
//					Futures.addCallback(respond, new FutureCallback<SendObject>() {
//						@Override
//						public void onSuccess(SendObject result) {
//							startActivity(new Intent(getApplicationContext(), LoggedInActivity.class));
//							Toast.makeText(getApplicationContext(), "Запись удалена", Toast.LENGTH_SHORT).show();
//							setAvailableUI(true);
//						}
//
//						@Override
//						public void onFailure(Throwable t) {
//							Toast.makeText(getApplicationContext(), ErrorService.ParseError(t.getLocalizedMessage()), Toast.LENGTH_SHORT).show();
//							setAvailableUI(true);
//						}
//					});
//
//					return true;
//				}
//			});
//		}
//	}
	
	private View.OnClickListener newListener = new OnClickListener() {
		@Override
		public void onClick(View v) {
			startActivity(new Intent(getApplicationContext(), TypeActivity.class));
		}
	};

	private View.OnClickListener logoutListener = new OnClickListener() {
		@Override
		public void onClick(View v) {
			startActivity(new Intent(getApplicationContext(), LoginActivity.class));
		}
	};
	
	public void setAvailableUI(int status){
		mCpvWait.setVisibility(status == AppData.UI_LOADING ? View.VISIBLE : View.GONE);
		mRvRecords.setVisibility(status == AppData.UI_VISIBLE ? View.VISIBLE : View.GONE);
		mLblProfile.setVisibility(status != AppData.UI_LOADING ? View.VISIBLE : View.GONE);
		mLblExit.setVisibility(status != AppData.UI_LOADING ? View.VISIBLE : View.GONE);
        mTxtNoItems.setVisibility(status == AppData.UI_NO_ITEMS ? View.VISIBLE : View.GONE);
 }
}
