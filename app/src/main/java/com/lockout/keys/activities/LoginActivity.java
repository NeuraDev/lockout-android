package com.lockout.keys.activities;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.github.rahatarmanahmed.cpv.CircularProgressView;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.lockout.keys.AskActivity;
import com.lockout.keys.Authentication;
import com.lockout.keys.ErrorDescription;
import com.lockout.keys.Pack;
import com.lockout.keys.R;
import com.lockout.keys.encrpytion.Encryptor;
import com.lockout.keys.helpers.AppData;
import com.lockout.keys.helpers.FileWork;
import com.lockout.keys.helpers.Keys;
import com.lockout.keys.helpers.Utilities;
import com.lockout.keys.http.ApiName;
import com.lockout.keys.http.RetrofitApi;
import com.lockout.keys.models.Auth1Output;
import com.lockout.keys.models.KeyWord;

import java.math.BigInteger;
import java.security.NoSuchAlgorithmException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import es.dmoral.toasty.Toasty;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginActivity extends AppCompatActivity {
	private final String TAG = "LoginActivity";

	private Button mBtnLogin;
	private EditText mTxtEmail;
	private EditText mTxtPassword;
	private EditText mTxtCodeword;
	private CircularProgressView mPbLoginWait;
	private TextView mLblRegister;
	private TextView mLblForgot;

	private LoginActivity mActivity;
	private ArrayList<String> timeArray;

	private long zeropoint;
	private String currentstring;

    private ScrollView mLayoutMain;

    @Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
		
		getWindow().requestFeature(Window.FEATURE_ACTION_BAR);
		setContentView(R.layout.custom_activity);
		
		mActivity = this;
		mLayoutMain = (ScrollView) findViewById(R.id.sv_login_main);
		mBtnLogin = (Button) findViewById(R.id.btnLogin);
		mTxtEmail = (EditText) findViewById(R.id.txtLoginEmail);
		mTxtPassword = (EditText) findViewById(R.id.txtLoginPassword);
		mTxtCodeword = (EditText) findViewById(R.id.txtLoginCodeword);
		mPbLoginWait = (CircularProgressView) findViewById(R.id.cpv_login_wait);

        mLayoutMain.requestFocus();

		mTxtCodeword.setOnEditorActionListener(new TextView.OnEditorActionListener() {
			@Override
			public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
				boolean handled = false;
				if (actionId == EditorInfo.IME_ACTION_GO) {
                  /* Write your logic here that will be executed when user taps next button */
					mBtnLogin.performClick();
					handled = true;
				}
				return handled;
			}
		});

		mLblForgot = (TextView) findViewById(R.id.lblLoginForgot);
		mLblRegister = (TextView) findViewById(R.id.lblLoginRegister);

		timeArray = new ArrayList<>();
		reinit();
		setAvailableUI(true);

		findViewById(R.id.alCustomActivity).setOnTouchListener(new View.OnTouchListener() {
			@Override
			public boolean onTouch(View v, MotionEvent event) {
				InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
				imm.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
				return false;
			}
		});

		mLblForgot.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				startActivity(new Intent(getApplicationContext(), ForgotActivity.class));
			}
		});
	
		mTxtCodeword.addTextChangedListener(new TextWatcher() {
			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
				// TODO Auto-generated method stub
			}

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
										  int after) {
				// TODO Auto-generated method stub
				long currenttime = System.currentTimeMillis();
				if (timeArray.size() == 0) {
					zeropoint = currenttime;
				}

				if (currenttime - zeropoint > 60000) {
					zeropoint = currenttime;
					Toasty.info(getApplicationContext(), getString(R.string.codeword_time_over), Toast.LENGTH_SHORT).show();
					reinit();
					return;
				}

				timeArray.add(String.valueOf(System.currentTimeMillis() - zeropoint));
			}

			@Override
			public void afterTextChanged(Editable s) {
				// TODO Auto-generated method stub
				if (currentstring.length() > mTxtCodeword.getText().length()) {
					Toasty.info(getApplicationContext(), getString(R.string.codeword_error), Toast.LENGTH_SHORT).show();
					reinit();
					return;
				}

				if (timeArray.size() != 0) {
					currentstring = mTxtCodeword.getText().toString();
					timeArray.add(String.valueOf(System.currentTimeMillis() - zeropoint));
				}
			}
		});

		mBtnLogin.setOnClickListener(loginClickListener);
		mLblRegister.setOnClickListener(registerClickListener);
	}

	private void reinit() {
		currentstring = "";
		mTxtCodeword.setText("");
		timeArray.clear();
		zeropoint = 0;
	}

	private boolean errorflag;

	View.OnClickListener loginClickListener = new OnClickListener(){
		 @Override
		 public void onClick(View v){
             if (mTxtPassword.getText().toString().equals("") ||
                     mTxtEmail.getText().toString().equals("") ||
                     mTxtCodeword.getText().toString().equals("")) {
                 Toast toast = Toast.makeText(getApplicationContext(), "Пожалуйста, заполните все поля", Toast.LENGTH_LONG);
                 toast.show();
                 return;
             }

             if (!(mTxtEmail.getText().toString().contains("@")) || !(mTxtEmail.getText().toString().contains("."))) {
                 Toast toast = Toast.makeText(getApplicationContext(), "Поле e-mail заполнено неверно", Toast.LENGTH_LONG);
                 toast.show();
                 return;
             }

             AppData.EMAIL = mTxtEmail.getText().toString();
             AppData.CODEWORD = mTxtCodeword.getText().toString();

             final SharedPreferences settings = getApplicationContext().getSharedPreferences("UserData", 0);
             final Auth1Output loginAuth = new Auth1Output();
             final Authentication authentication = new Authentication();

             loginAuth.i = mTxtEmail.getText().toString();
             authentication.RandomString();

             settings.edit().clear()
                     .putString("email", mTxtEmail.getText().toString())
                     .putString("codeword", mTxtCodeword.getText().toString())
                     .putString("password", mTxtPassword.getText().toString())
                     .apply();

             final Auth1Output auth = new Auth1Output();
             setAvailableUI(false);

             final RetrofitApi api = new RetrofitApi(getApplicationContext());
             final RetrofitApi.PostJson apiCall = api.getRetrofitApi().create(RetrofitApi.PostJson.class);
             apiCall.post(ApiName.AUTH_START, (JsonObject) new Gson().toJsonTree(loginAuth)).enqueue(new Callback<JsonObject>() {
                 @Override
                 public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                     if (response.body() == null) {
                         Toasty.error(getApplicationContext(), getString(R.string.error_internal), Toast.LENGTH_SHORT).show();
                         setAvailableUI(true);
                     } else {
                         Auth1Output result = new Gson().fromJson(response.body(), Auth1Output.class);

                         auth.s = result.s;
                         authentication.getN(mTxtEmail.getText().toString(), result.s);
                         loginAuth.a = authentication.getA().toString();

                         apiCall.post(ApiName.AUTH_SECOND, (JsonObject) new Gson().toJsonTree(loginAuth))
                                 .enqueue(new Callback<JsonObject>() {
                                     @Override public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                                         if (response.body() == null) {
                                             Toasty.error(getApplicationContext(), getString(R.string.error_internal), Toast.LENGTH_SHORT).show();
                                             setAvailableUI(true);
                                         } else {
                                             Auth1Output result = new Gson().fromJson(response.body(), Auth1Output.class);

                                             auth.b = result.b;
                                             authentication.getU(auth.b);
                                             String x = authentication.getX(mTxtPassword.getText().toString(), auth.s).toString();
                                             authentication.getk();
                                             authentication.getS(auth.b, authentication.g.toString(), x);
                                             authentication.getK().toString();
                                             loginAuth.b = auth.b;
                                             authentication.getV(x);

                                             AppData.DEVICE_ID = Settings.Secure.getString(getApplicationContext().getContentResolver(),
                                                     Settings.Secure.ANDROID_ID);

                                             settings.edit()
                                                     .putString("deviceid", AppData.DEVICE_ID)
                                                     .putString("sendIv", Encryptor.generateIv(AppData.DEVICE_ID))
                                                     .apply();

                                             loginAuth.id_device = AppData.DEVICE_ID;
                                             loginAuth.m = authentication.getM(authentication.N,
                                                     authentication.g, mTxtEmail.getText().toString(),
                                                     auth.s, authentication.A, auth.b, authentication.K).toString();

                                             apiCall.post("tables/Auth2", (JsonObject) new Gson().toJsonTree(loginAuth))
                                                     .enqueue(new Callback<JsonObject>() {
                                                         @Override
                                                         public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                                                             Auth1Output result = new Gson().fromJson(response.body(), Auth1Output.class);

                                                             BigInteger myR = authentication.getR(authentication.A, authentication.M, authentication.K);
                                                             BigInteger serverR = new BigInteger(result.r);

                                                             if (myR.equals(serverR)) {
                                                                 FileWork readfile = new FileWork(getApplicationContext());

                                                                 try {
                                                                     AppData.LOG_SALT = Encryptor.getPBKDF2(result.id, mTxtEmail.getText().toString());
                                                                     AppData.SALT = Encryptor.getPBKDF2(result.id, mTxtPassword.getText().toString());
                                                                 } catch (Exception e) {
                                                                     Log.e("Error", e.getLocalizedMessage());
                                                                     errorflag = true;
                                                                 }

                                                                 AppData.USER_ID = result.id;

                                                                 settings.edit()
                                                                         .putString("username", result.username)
                                                                         .putString("userid", result.id)
                                                                         .putString("tokenpart", result.r)
                                                                         .putString("serverOpenKey", readfile.readFromFile(result.id + "sok"))
                                                                         .putString("clientKeyPair", readfile.readFromFile(result.id + "ckp"))
                                                                         .putString("logsalt", AppData.LOG_SALT)
                                                                         .putString("salt", AppData.SALT)
                                                                         .apply();

                                                                 SimpleDateFormat dateFormatUTC = new SimpleDateFormat("dd.MM.yyyy HH:mm:ss");

                                                                 final KeyWord keyWord = new KeyWord();
                                                                 try {
                                                                     keyWord.setData(Encryptor.encrypt(AppData.LOG_SALT, Encryptor.MD5(mTxtEmail.getText().toString()), timeArray.toString()));
                                                                     keyWord.setDeviceid(Encryptor.encrypt(AppData.LOG_SALT, Encryptor.MD5(mTxtEmail.getText().toString()), AppData.DEVICE_ID));
                                                                     keyWord.setUserid(result.id);
                                                                     keyWord.setDate(Encryptor.encrypt(AppData.LOG_SALT, Encryptor.MD5(mTxtEmail.getText().toString()), dateFormatUTC.format(new Date())));
                                                                     keyWord.setKeyword(Encryptor.encrypt(AppData.LOG_SALT, Encryptor.MD5(mTxtEmail.getText().toString()), mTxtCodeword.getText().toString()));
                                                                 } catch (NoSuchAlgorithmException e) {
                                                                     Log.e("Error", e.toString());
                                                                 }

                                                                 final Pack pack = new Pack(getApplicationContext());
                                                                 apiCall.post("tables/Auth3", pack.packing(keyWord))
                                                                         .enqueue(new Callback<JsonObject>() {
                                                                             @Override
                                                                             public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                                                                                 if (!Utilities.validateCode(response.code())) {
                                                                                     settings.edit()
                                                                                        .putString("clientKeyPair", null)
                                                                                        .putString("serverOpenKey", null)
                                                                                        .apply();
                                                                                     Keys.startExchange(mActivity, keyWord);
                                                                                 } else {
                                                                                     ErrorDescription desc = pack.unpacking(response.body(), ErrorDescription.class);
                                                                                     if (desc.Description.equals(getString(R.string.server_ask))) {
                                                                                         setAvailableUI(true);
                                                                                         startActivity(new Intent(getApplicationContext(), AskActivity.class));
                                                                                     } else {
                                                                                         setAvailableUI(true);
                                                                                         startActivity(new Intent(getApplicationContext(), LoggedInActivity.class));
                                                                                     }
                                                                                 }
                                                                             }

                                                                             @Override
                                                                             public void onFailure(Call<JsonObject> call, Throwable t) {
                                                                                 Toasty.error(getApplicationContext(), getString(R.string.error_credentials), Toast.LENGTH_SHORT).show();
                                                                                 setAvailableUI(true);
                                                                             }
                                                                         });
                                                             } else {
                                                                 Toasty.error(getApplicationContext(), getString(R.string.error_credentials), Toast.LENGTH_SHORT).show();
                                                                 setAvailableUI(true);
                                                             }
                                                         }

                                                         @Override
                                                         public void onFailure(Call<JsonObject> call, Throwable t) {
                                                             Toasty.error(getApplicationContext(), t.getLocalizedMessage(), Toast.LENGTH_SHORT).show();
                                                             setAvailableUI(true);
                                                         }
                                                     });
                                         }
                                     }

                                     @Override
                                     public void onFailure(Call<JsonObject> call, Throwable t) {
                                         Toasty.error(getApplicationContext(), t.getLocalizedMessage(), Toast.LENGTH_SHORT).show();
                                         setAvailableUI(true);
                                     }
                                 });
                     }
                 }

                 @Override
                 public void onFailure(Call<JsonObject> call, Throwable t) {
                     Log.e(TAG, "error - " + t.getLocalizedMessage());
                     Toasty.error(getApplicationContext(), t.getLocalizedMessage(), Toast.LENGTH_SHORT).show();
                     setAvailableUI(true);
                 }
             });
		 }
	 };
	 
	 public void setAvailableUI(boolean v){
         mTxtPassword.setEnabled(v);
         mTxtEmail.setEnabled(v);
         mTxtCodeword.setEnabled(v);
         mLblForgot.setEnabled(v);
         mLblRegister.setEnabled(v);
         mLblRegister.setClickable(v);
         mLblForgot.setClickable(v);
         mPbLoginWait.setVisibility(v ? View.GONE : View.VISIBLE);
         mBtnLogin.setVisibility(v ? View.VISIBLE : View.GONE);
	 }
	 
	 View.OnClickListener registerClickListener = new OnClickListener() {
		
		@Override
		public void onClick(View v) {
			Intent registerIntent = new Intent(getApplicationContext(), RegisterActivity.class);
			startActivity(registerIntent);
		}
	};
}


