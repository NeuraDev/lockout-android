package com.lockout.keys.activities;

import android.app.ActionBar;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.ClipboardManager;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.github.rahatarmanahmed.cpv.CircularProgressView;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.lockout.keys.ErrorService;
import com.lockout.keys.R;
import com.lockout.keys.RegistrationTool;
import com.lockout.keys.activities.LoginActivity;
import com.lockout.keys.helpers.AppData;
import com.lockout.keys.helpers.Utilities;
import com.lockout.keys.http.RetrofitApi;
import com.lockout.keys.models.Account;
import com.lockout.keys.models.KeyWord;
import com.squareup.okhttp.MediaType;
import com.squareup.okhttp.RequestBody;
import com.squareup.okhttp.ResponseBody;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import es.dmoral.toasty.Toasty;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

public class RegisterActivity extends AppCompatActivity {
	private final String TAG = "RegisterActivity";
	private EditText mTxtUsername;
	private EditText mTxtPassword;
	private EditText mTxtConfirm;
	private EditText mTxtEmail;
	private EditText mTxtCodeword;
	private EditText mTxtPhone;
	private Button mBtnRegister;

	private CheckBox chkOTP;
	private CheckBox chkGrid;

	private Activity mActivity;
	private ArrayList<String> timeArray;
	private long zeropoint;
	private String currentstring;
	private CircularProgressView mCpvWait;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_register_account);
		getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
		
		mActivity = this;
				
		//Get UI elements
		mTxtUsername = (EditText) findViewById(R.id.txtRegisterUsername);
		mTxtPassword = (EditText) findViewById(R.id.txtRegisterPassword);
	    mTxtConfirm = (EditText) findViewById(R.id.txtRegisterConfirm);
		mTxtEmail = (EditText) findViewById(R.id.txtRegisterEmail);
		mTxtCodeword = (EditText) findViewById(R.id.txtRegisterCodeword);
		mCpvWait = (CircularProgressView) findViewById(R.id.cpv_register_wait);
		mTxtPhone = (EditText) findViewById(R.id.txtRegisterPhone);
		mBtnRegister = (Button) findViewById(R.id.btnRegisterOk);
		chkOTP = (CheckBox) findViewById(R.id.chkRegisterOTP);
		chkGrid = (CheckBox) findViewById(R.id.chkRegisterGrid);

		mBtnRegister.setOnClickListener(registerClickListener);

		Toolbar toolbar = (Toolbar) findViewById(R.id.tb_register_activity);
		setSupportActionBar(toolbar);

		if (getSupportActionBar() != null) {
			getSupportActionBar().setDisplayHomeAsUpEnabled(true);
			getSupportActionBar().setDisplayShowHomeEnabled(true);
		}

		Utilities.setupHideKeyboard(this, findViewById(R.id.cl_register_main));

		timeArray = new ArrayList<>();
		reinit();
		mCpvWait.setVisibility(View.GONE);
		setAvailableUI(true);
		//Set Click Listeners
	     
	     mTxtCodeword.addTextChangedListener(new TextWatcher() {
				@Override public void onTextChanged(CharSequence s, int start, int before, int count) {}
				@Override public void beforeTextChanged(CharSequence s, int start, int count,
						int after) {
					long currenttime = System.currentTimeMillis();
					if (timeArray.size() == 0) {
						zeropoint = currenttime;
					}

					if (currenttime - zeropoint > (60 * 1000)) {
						zeropoint = currenttime;
						Toasty.info(getApplicationContext(), getString(R.string.codeword_time_over), Toast.LENGTH_SHORT).show();
						reinit();
						return;
					}

					timeArray.add(String.valueOf(System.currentTimeMillis() - zeropoint));
				}
				
				@Override public void afterTextChanged(Editable s) {
					if (currentstring.length() > mTxtCodeword.getText().length()) {
						Toasty.info(getApplicationContext(), getString(R.string.codeword_error), Toast.LENGTH_SHORT).show();
						reinit();
						return;
					}

					if (timeArray.size() != 0) {
						currentstring = mTxtCodeword.getText().toString();
						timeArray.add(String.valueOf(System.currentTimeMillis() - zeropoint));
					}
				}
			});
	}

	@Override public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
			case android.R.id.home:
				finish();
				break;

			default:
				break;
		}

		return super.onOptionsItemSelected(item);
	}

	private void reinit() {
		currentstring = "";
		mTxtCodeword.setText("");
		timeArray.clear();
		zeropoint = 0;
	}

	public void setAvailableUI(boolean v){
		mTxtUsername.setEnabled(v);
		mTxtUsername.setClickable(v);
		mTxtPassword.setEnabled(v);
		mTxtPassword.setClickable(v);
		mTxtConfirm.setEnabled(v);
		mTxtConfirm.setClickable(v);
		mTxtEmail.setEnabled(v);
		mTxtEmail.setClickable(v);
		mTxtPhone.setClickable(v);
		mTxtPhone.setEnabled(v);
		mTxtCodeword.setEnabled(v);
		mTxtCodeword.setClickable(v);
		chkOTP.setEnabled(v);
		chkOTP.setClickable(v);
		chkGrid.setEnabled(v);
		chkGrid.setClickable(v);
		mCpvWait.setVisibility(v? View.GONE : View.VISIBLE);
		mBtnRegister.setVisibility(v ? View.VISIBLE : View.GONE);
	 }

	View.OnClickListener registerClickListener = new OnClickListener() {
		
		@Override
		public void onClick(View v) {
			if (mTxtUsername.getText().toString().equals("") ||
					mTxtPassword.getText().toString().equals("") ||
					mTxtConfirm.getText().toString().equals("") ||
					mTxtEmail.getText().toString().equals("") ||
					mTxtCodeword.getText().toString().equals("")) {
				Toasty.warning(getApplicationContext(), getString(R.string.info_all_fields), Toast.LENGTH_SHORT).show();
                return;
			}

			if (!mTxtPassword.getText().toString().equals(mTxtConfirm.getText().toString())) {
				Toasty.warning(getApplicationContext(), getString(R.string.info_passwords_break), Toast.LENGTH_SHORT).show();
                return;
			}

			if (!(mTxtEmail.getText().toString().contains("@")) || !(mTxtEmail.getText().toString().contains("."))) {
				Toasty.warning(getApplicationContext(), getString(R.string.info_email_break), Toast.LENGTH_SHORT).show();
                return;
			}

            setAvailableUI(false);
            final Account account = new Account();
            RegistrationTool registration = new RegistrationTool();

            registration.getX(mTxtPassword.getText().toString());
            registration.getN(mTxtEmail.getText().toString());

            account.email = mTxtEmail.getText().toString();
            account.salt = registration.s;
            account.codeword = mTxtCodeword.getText().toString();
            account.username = mTxtUsername.getText().toString();
            account.phone = mTxtPhone.getText().toString();
            account.password = registration.getV().toString();
            account.GridLink = chkGrid.isChecked() ? "YES" : "NO";

            final RetrofitApi api = new RetrofitApi(getApplicationContext());
            api.getRetrofitApi().create(RetrofitApi.PostRequest.class).post("tables/Reg",
                    (JsonObject) new Gson().toJsonTree(account)).enqueue(new Callback<String>() {
                        @Override public void onResponse(Call<String> call, Response<String> response) {
                            SimpleDateFormat dateFormatUTC = new SimpleDateFormat("dd.MM.yyyy HH:mm:ss");
                            KeyWord keyWord = new KeyWord();
							keyWord.setData(timeArray.toString());
                            keyWord.setDeviceid(AppData.DEVICE_ID);
                            keyWord.setUserid("");
                            keyWord.setDate(dateFormatUTC.format(new Date()));
                            keyWord.setKeyword(mTxtCodeword.getText().toString());

                            api.getRetrofitApi().create(RetrofitApi.PostRequest.class).post("tables/RegKeyWord",
                                    (JsonObject) new Gson().toJsonTree(keyWord)).enqueue(new Callback<String>() {
                                @Override
                                public void onResponse(Call<String> call, Response<String> response) {
                                    if (chkOTP.isChecked()) {
                                        ClipboardManager clipboard = (ClipboardManager) getSystemService(CLIPBOARD_SERVICE);
                                        clipboard.setText(response.body());
                                        Toasty.success(getApplicationContext(), getString(R.string.register_success_otp), Toast.LENGTH_SHORT).show();
                                    } else {
                                        Toasty.success(getApplicationContext(), getString(R.string.register_success), Toast.LENGTH_SHORT).show();
                                    }
                                    setAvailableUI(true);
                                    Intent backIntent = new Intent(getApplicationContext(), LoginActivity.class);
                                    startActivity(backIntent);
                                }

                                @Override
                                public void onFailure(Call<String> call, Throwable t) {
                                    Toasty.error(getApplicationContext(), t.getLocalizedMessage(), Toast.LENGTH_SHORT).show();
                                    setAvailableUI(true);
                                }
                            });
                        }

                        @Override public void onFailure(Call<String> call, Throwable t) {
                            Toasty.error(getApplicationContext(), t.getLocalizedMessage(), Toast.LENGTH_SHORT).show();
                            setAvailableUI(true);
                        }
                    });
		}
	};
}
