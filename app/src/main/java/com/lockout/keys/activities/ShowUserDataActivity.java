package com.lockout.keys.activities;

import android.app.ActionBar;
import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.ClipboardManager;
import android.text.Editable;
import android.text.TextWatcher;
import android.text.method.PasswordTransformationMethod;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.github.rahatarmanahmed.cpv.CircularProgressView;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.lockout.keys.ErrorService;
import com.lockout.keys.Pack;
import com.lockout.keys.R;
import com.lockout.keys.activities.LoggedInActivity;
import com.lockout.keys.encrpytion.Encryptor;
import com.lockout.keys.helpers.AppData;
import com.lockout.keys.helpers.Utilities;
import com.lockout.keys.http.ApiName;
import com.lockout.keys.http.RetrofitApi;
import com.lockout.keys.models.DataTable;
import com.lockout.keys.models.HeadersListModel;
import com.lockout.keys.models.RelationDataModel;
import com.lockout.keys.models.SendObject;

import es.dmoral.toasty.Toasty;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

public class ShowUserDataActivity extends AppCompatActivity {
    private final String TAG = "SHOWUSERDATA";
    private Button mBtnSave;
    private EditText mTxtLogin;
    private EditText mTxtLink;
    private EditText mTxtPassword;
    private EditText mTxtName;
    private Spinner mSpShowData;

    private boolean isPassworded;
    private TextView mBtnDelete;

    private SharedPreferences settings;
    private Pack pack;
    private CircularProgressView mCpvWait;
    private RetrofitApi.PostJson mApiCall;
    private CollapsingToolbarLayout mCtbMain;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_data);

        Toolbar toolbar = (Toolbar) findViewById(R.id.tb_data_activity);
        setSupportActionBar(toolbar);

        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }

        Retrofit client = new RetrofitApi(getApplicationContext()).getRetrofitApi();
        mApiCall = client.create(RetrofitApi.PostJson.class);

        Utilities.setupHideKeyboard(this, findViewById(R.id.cl_data_main));
        Utilities.setupHideKeyboard(this, findViewById(R.id.nested_data));

        settings = getSharedPreferences("UserData", 0);
        pack = new Pack(getApplicationContext());

        mBtnSave = (Button) findViewById(R.id.btnProcessData);

        mCtbMain = (CollapsingToolbarLayout) findViewById(R.id.ctb_data_activity);

        mTxtLogin = (EditText) findViewById(R.id.txtDataLogin);
        mTxtLink = (EditText) findViewById(R.id.txtDataLink);
        mTxtName = (EditText) findViewById(R.id.txtDataName);
        mTxtPassword = (EditText) findViewById(R.id.txtDataPassword);
        mSpShowData = (Spinner) findViewById(R.id.spDataSecure);
        mBtnDelete = (TextView) findViewById(R.id.btnDataDelete);
        mCpvWait = (CircularProgressView) findViewById(R.id.cpv_data_wait);

        isPassworded = true;

        ImageView mCopyLogin = (ImageView) findViewById(R.id.ivDataCopy);
        ImageView mCopyPsw = (ImageView) findViewById(R.id.ivPsvCopy);
        ImageView mShowPsw = (ImageView) findViewById(R.id.ivPsvShow);

        mBtnDelete.setOnClickListener(deleteClickListener);

        mTxtName.addTextChangedListener(new TextWatcher() {
            @Override public void beforeTextChanged(CharSequence s, int start, int count, int after) {}
            @Override public void onTextChanged(CharSequence s, int start, int before, int count) {}
            @Override public void afterTextChanged(Editable s) {
                mCtbMain.setTitle(s.toString().equals("") ? getString(R.string.title_activity_show_user_data) : s.toString());
            }
        });

        mCopyLogin.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                ClipboardManager clipboard = (ClipboardManager) getSystemService(CLIPBOARD_SERVICE);
                clipboard.setText(mTxtLogin.getText().toString());
                Toasty.success(getApplicationContext(), getString(R.string.copy_login), Toast.LENGTH_SHORT).show();
            }
        });

        mCopyPsw.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                ClipboardManager clipboard = (ClipboardManager) getSystemService(CLIPBOARD_SERVICE);
                clipboard.setText(mTxtPassword.getText().toString());
                Toasty.success(getApplicationContext(), getString(R.string.copy_password), Toast.LENGTH_SHORT).show();
            }
        });

        mShowPsw.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                mTxtPassword.setTransformationMethod(isPassworded ? null : PasswordTransformationMethod.getInstance());
                isPassworded = !isPassworded;
            }
        });

        mBtnSave.setOnClickListener(saveClickListener);
        mBtnSave.setText(getString(R.string.title_save));
        loadData();
    }

    @Override public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                startActivity(new Intent(getApplicationContext(), LoggedInActivity.class));
                finish();
                break;

            default:
                break;
        }

        return super.onOptionsItemSelected(item);
    }

    public void loadData(){
        setAvailable(false);
        HeadersListModel hListModel = new HeadersListModel();
        hListModel.dataid = AppData.CURRENT_HEADER.getDataid();
        hListModel.deviceid = AppData.DEVICE_ID;
        mCtbMain.setTitle(AppData.CURRENT_HEADER.getNameinfo());

        mApiCall.post(ApiName.LOAD_DATA, pack.packing(hListModel))
                .enqueue(new Callback<JsonObject>() {
                    @Override
                    public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                        if (Utilities.validateCode(response.code())) {
                            DataTable dataTable = new Pack(getApplicationContext())
                                    .unpacking(response.body(), DataTable.class);
                            mTxtName.setText(AppData.CURRENT_HEADER.getNameinfo());
                            mSpShowData.setSelection(Integer.valueOf(AppData.CURRENT_HEADER.getSecurelvl()));
                            mTxtLink.setText(Encryptor.decrypt(AppData.SALT,
                                    Encryptor.generateIv(AppData.EMAIL), dataTable.urlinfo));
                            mTxtLogin.setText(Encryptor.decrypt(AppData.SALT,
                                    Encryptor.generateIv(AppData.EMAIL), dataTable.loginfo));
                            mTxtPassword.setText(Encryptor.decrypt(AppData.SALT,
                                    Encryptor.generateIv(AppData.EMAIL), dataTable.passinfo));

                            setAvailable(true);
                        } else {
                            setAvailable(true);
                            Toasty.error(getApplicationContext(), "Status code - " + response.code(), Toast.LENGTH_SHORT).show();
                            startActivity(new Intent(getApplicationContext(), LoggedInActivity.class));
                        }
                    }

                    @Override
                    public void onFailure(Call<JsonObject> call, Throwable t) {
                        setAvailable(true);
                        Toasty.error(getApplicationContext(), t.getLocalizedMessage(), Toast.LENGTH_SHORT).show();
                        finish();
                        startActivity(new Intent(getApplicationContext(), LoggedInActivity.class));
                    }
                });
    }

    public void setAvailable(boolean v) {
        mTxtPassword.setClickable(v);
        mTxtPassword.setEnabled(v);
        mTxtLogin.setClickable(v);
        mTxtLogin.setEnabled(v);
        mTxtLink.setClickable(v);
        mTxtLink.setEnabled(v);
        mTxtName.setEnabled(v);
        mTxtName.setClickable(v);
        mBtnSave.setClickable(v);
        mBtnSave.setEnabled(v);
        mBtnDelete.setEnabled(v);
        mBtnDelete.setClickable(v);
        mSpShowData.setEnabled(v);
        mSpShowData.setClickable(v);
        mBtnSave.setVisibility(v ? View.VISIBLE : View.GONE);
        mCpvWait.setVisibility(v ? View.GONE : View.VISIBLE);
    }

    View.OnClickListener deleteClickListener = new OnClickListener() {
        @Override
        public void onClick(View view) {
            setAvailable(false);
            RelationDataModel rDataModel = new RelationDataModel();
            rDataModel.dataid = AppData.CURRENT_HEADER.getDataid();
            rDataModel.typeinfo = getString(R.string.type_data);

            mApiCall.post(ApiName.DELETE_DATA, pack.packing(rDataModel))
                    .enqueue(new Callback<JsonObject>() {
                        @Override
                        public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                            Toasty.success(getApplicationContext(), getString(R.string.deleted_data), Toast.LENGTH_SHORT).show();
                            startActivity(new Intent(getApplicationContext(), LoggedInActivity.class));
                        }

                        @Override
                        public void onFailure(Call<JsonObject> call, Throwable t) {
                            setAvailable(true);
                            Toast.makeText(getApplicationContext(), getString(R.string.deleted_error), Toast.LENGTH_SHORT).show();
                        }
                    });
        }
    };

    View.OnClickListener saveClickListener = new OnClickListener() {
        @Override
        public void onClick(View v) {
            setAvailable(false);
            if (mTxtName.getText().toString().equals("") || mTxtPassword.getText().toString().equals("") ||
                    mTxtLink.getText().toString().equals("") || mTxtLogin.getText().toString().equals("")
                    || (mSpShowData.getSelectedItemPosition() < 0)) {
                Toasty.warning(getApplicationContext(), getString(R.string.info_all_fields), Toast.LENGTH_SHORT).show();
                return;
            }

            RelationDataModel rDataModel = new RelationDataModel();
            rDataModel.dataid = AppData.CURRENT_HEADER.getDataid();
            rDataModel.userid = AppData.CURRENT_HEADER.getUserid();
            rDataModel.nameinfo = mTxtName.getText().toString();
            rDataModel.loginfo = Encryptor.encrypt(settings.getString("salt", "null"),
                    Encryptor.generateIv(AppData.EMAIL), mTxtLogin.getText().toString());
            rDataModel.urlinfo = Encryptor.encrypt(settings.getString("salt", "null"),
                    Encryptor.generateIv(AppData.EMAIL), mTxtLink.getText().toString());
            rDataModel.passinfo = Encryptor.encrypt(settings.getString("salt", "null"),
                    Encryptor.generateIv(AppData.EMAIL), mTxtPassword.getText().toString());
            rDataModel.securelvl = String.valueOf(mSpShowData.getSelectedItemPosition());
            rDataModel.typeinfo = getString(R.string.type_data);

            mApiCall.post(ApiName.UPDATE_DATA, pack.packing(rDataModel))
                    .enqueue(new Callback<JsonObject>() {
                        @Override
                        public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                            Toasty.success(getApplicationContext(), getString(R.string.updated_data), Toast.LENGTH_SHORT).show();
                            startActivity(new Intent(getApplicationContext(), LoggedInActivity.class));
                        }

                        @Override
                        public void onFailure(Call<JsonObject> call, Throwable t) {
                            setAvailable(true);
                            Toasty.error(getApplicationContext(), ErrorService.ParseError(t.getLocalizedMessage()), Toast.LENGTH_SHORT).show();
                        }
                    });
        }
    };
}
