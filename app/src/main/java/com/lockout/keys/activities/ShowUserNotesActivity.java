package com.lockout.keys.activities;

import android.app.ActionBar;
import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.github.rahatarmanahmed.cpv.CircularProgressView;
import com.google.common.util.concurrent.FutureCallback;
import com.google.common.util.concurrent.Futures;
import com.google.common.util.concurrent.ListenableFuture;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.lockout.keys.ErrorService;
import com.lockout.keys.NoteTable;
import com.lockout.keys.Pack;
import com.lockout.keys.R;
import com.lockout.keys.activities.LoggedInActivity;
import com.lockout.keys.encrpytion.Encryptor;
import com.lockout.keys.helpers.AppData;
import com.lockout.keys.helpers.Utilities;
import com.lockout.keys.http.ApiName;
import com.lockout.keys.http.RetrofitApi;
import com.lockout.keys.models.HeadersListModel;
import com.lockout.keys.models.RelationDataModel;
import com.lockout.keys.models.SendObject;

import es.dmoral.toasty.Toasty;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;


public class ShowUserNotesActivity extends AppCompatActivity {
	private Activity mActivity;
	private EditText mTxtName;
	private EditText mTxtText;
	private Button mBtnSave;
	private String TAG = "ShowUserNotesActivity";
	private Spinner mSpShowNote;
	private TextView mBtnDelete;
	private Pack pack;

	private CircularProgressView mCpvWait;
	private RetrofitApi.PostJson mApiCall;
	private CollapsingToolbarLayout mCtbMain;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
		setContentView(R.layout.activity_note);

		mActivity = this;

		Retrofit client = new RetrofitApi(getApplicationContext()).getRetrofitApi();
		mApiCall = client.create(RetrofitApi.PostJson.class);

		Toolbar toolbar = (Toolbar) findViewById(R.id.tb_note_activity);
		setSupportActionBar(toolbar);

		if (getSupportActionBar() != null) {
			getSupportActionBar().setDisplayHomeAsUpEnabled(true);
			getSupportActionBar().setDisplayShowHomeEnabled(true);
		}

		Utilities.setupHideKeyboard(this, findViewById(R.id.cl_note_main));
		Utilities.setupHideKeyboard(this, findViewById(R.id.nested_note));

		mCtbMain = (CollapsingToolbarLayout) findViewById(R.id.ctb_note_activity);
		mTxtName = (EditText) findViewById(R.id.txtNoteName);
		mTxtText = (EditText) findViewById(R.id.txtNoteText);
		mSpShowNote = (Spinner) findViewById(R.id.spNoteSecure);
		mBtnSave = (Button) findViewById(R.id.btnNoteProcess);
		mBtnDelete = (TextView) findViewById(R.id.btnNoteDelete);
		mCpvWait = (CircularProgressView) findViewById(R.id.cpv_note_wait);
		pack = new Pack(getApplicationContext());
		mBtnSave.setText(getString(R.string.title_save));

        mBtnSave.setOnClickListener(saveClickListener);
		mBtnDelete.setOnClickListener(deleteClickListener);
        mBtnDelete.setVisibility(View.VISIBLE);

		mTxtName.addTextChangedListener(new TextWatcher() {
			@Override public void beforeTextChanged(CharSequence s, int start, int count, int after) {}
			@Override public void onTextChanged(CharSequence s, int start, int before, int count) {}
			@Override public void afterTextChanged(Editable s) {
				mCtbMain.setTitle(s.toString().equals("") ? getString(R.string.title_activity_add_new_data) : s.toString());
			}
		});

		loadData();
	}

	@Override public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
			case android.R.id.home:
				startActivity(new Intent(getApplicationContext(), LoggedInActivity.class));
				finish();
				break;

			default:
				break;
		}

		return super.onOptionsItemSelected(item);
	}

	public void loadData() {
		setAvailable(false);
		HeadersListModel hListModel = new HeadersListModel();
		hListModel.dataid = AppData.CURRENT_HEADER.getDataid();
		hListModel.deviceid = AppData.DEVICE_ID;
		mCtbMain.setTitle(AppData.CURRENT_HEADER.getNameinfo());

		mApiCall.post(ApiName.LOAD_NOTE, pack.packing(hListModel))
				.enqueue(new Callback<JsonObject>() {
					@Override
					public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                        if (Utilities.validateCode(response.code())) {
                            NoteTable noteTable = pack.unpacking(response.body(), NoteTable.class);
                            mTxtName.setText(AppData.CURRENT_HEADER.getNameinfo());
                            mSpShowNote.setSelection(Integer.valueOf(AppData.CURRENT_HEADER.getSecurelvl()));
                            mTxtText.setText(Encryptor.decrypt(AppData.SALT, Encryptor.generateIv(AppData.EMAIL),
                                    noteTable.noteinfo));
                            setAvailable(true);
                        } else {
                            setAvailable(true);
                            Toasty.error(getApplicationContext(), "Status code - " + response.code(), Toast.LENGTH_SHORT).show();
                            startActivity(new Intent(getApplicationContext(), LoggedInActivity.class));
                        }
					}

					@Override
					public void onFailure(Call<JsonObject> call, Throwable t) {
						setAvailable(true);
						Toasty.error(getApplicationContext(), t.getLocalizedMessage(), Toast.LENGTH_SHORT).show();
						startActivity(new Intent(getApplicationContext(), LoggedInActivity.class));
					}
				});
	}
		
	View.OnClickListener cancelNoteListener = new OnClickListener() {
		
		@Override
		public void onClick(View v) {
			startActivity(new Intent(getApplicationContext(), LoggedInActivity.class));
		}
	};
	
	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		if (keyCode == KeyEvent.KEYCODE_BACK && event.getRepeatCount() == 0) {
			startActivity(new Intent(getApplicationContext(), LoggedInActivity.class));
			return true;
		}
		return super.onKeyDown(keyCode, event);		
	}
	
	public void setAvailable(boolean v) {
		mTxtName.setEnabled(v);
		mTxtName.setClickable(v);
		mTxtText.setEnabled(v);
		mTxtText.setClickable(v);
		mSpShowNote.setEnabled(v);
		mSpShowNote.setClickable(v);

        mBtnSave.setVisibility(v ? View.VISIBLE : View.GONE);
        mBtnDelete.setVisibility(v ? View.VISIBLE : View.GONE);
		mCpvWait.setVisibility(v ? View.GONE : View.VISIBLE);
	}

	private View.OnClickListener deleteClickListener = new OnClickListener() {
        @Override
        public void onClick(View view) {
            setAvailable(false);

            RelationDataModel rDataModel = new RelationDataModel();
            rDataModel.dataid = AppData.CURRENT_HEADER.getDataid();
            rDataModel.typeinfo = getString(R.string.type_note);

            mApiCall.post(ApiName.DELETE_NOTE, pack.packing(rDataModel))
                    .enqueue(new Callback<JsonObject>() {
                        @Override
                        public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                            Toasty.success(getApplicationContext(), getString(R.string.deleted_note), Toast.LENGTH_SHORT).show();
                            startActivity(new Intent(getApplicationContext(), LoggedInActivity.class));
                            setAvailable(true);
                        }

                        @Override
                        public void onFailure(Call<JsonObject> call, Throwable t) {
                            setAvailable(true);
                            Toast.makeText(getApplicationContext(), getString(R.string.deleted_error), Toast.LENGTH_SHORT).show();
                        }
                    });
        }
    };
	
	private View.OnClickListener saveClickListener = new OnClickListener() {
		@Override
		public void onClick(View v) {
			if (mTxtName.getText().toString().equals("") || mTxtText.getText().toString().equals("")
                    || (mSpShowNote.getSelectedItemPosition() < 0)) {
				Toasty.warning(getApplicationContext(), getString(R.string.info_all_fields), Toast.LENGTH_SHORT).show();
				return;
			}

			setAvailable(false);
			RelationDataModel rDataModel = new RelationDataModel();
			rDataModel.dataid = AppData.CURRENT_HEADER.getDataid();
            rDataModel.userid = AppData.USER_ID;
			rDataModel.nameinfo = mTxtName.getText().toString();
			rDataModel.noteinfo = Encryptor.encrypt(AppData.SALT, Encryptor.generateIv(AppData.EMAIL),
                    mTxtText.getText().toString());
			rDataModel.securelvl = String.valueOf(mSpShowNote.getSelectedItemPosition());
			rDataModel.typeinfo = getString(R.string.type_note);


			Pack pack = new Pack(getApplicationContext());
            mApiCall.post(ApiName.UPDATE_NOTE, pack.packing(rDataModel))
                    .enqueue(new Callback<JsonObject>() {
                        @Override
                        public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                            setAvailable(true);
                            Toasty.success(getApplicationContext(), getString(R.string.updated_note), Toast.LENGTH_SHORT).show();
                            startActivity(new Intent(getApplicationContext(), LoggedInActivity.class));
                        }

                        @Override
                        public void onFailure(Call<JsonObject> call, Throwable t) {
                            setAvailable(true);
                            Toasty.error(getApplicationContext(), ErrorService.ParseError(t.getLocalizedMessage()), Toast.LENGTH_SHORT).show();
                        }
                    });
		}
	};
}
