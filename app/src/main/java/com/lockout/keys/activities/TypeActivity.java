package com.lockout.keys.activities;

import android.app.ActionBar;
import android.app.ListActivity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.lockout.keys.R;
import com.lockout.keys.activities.AddNewDataActivity;
import com.lockout.keys.activities.AddNewNoteActivity;
import com.lockout.keys.activities.LoggedInActivity;
import com.lockout.keys.adapters.TypeAdapter;
import com.lockout.keys.helpers.ListConfig;
import com.lockout.keys.models.Model;
import com.lockout.keys.models.Type;

import java.util.ArrayList;

/**
 * Created by sunwi on 05.12.2015.
 */
public class TypeActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_type);

        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        RecyclerView mRvType = (RecyclerView) findViewById(R.id.recycler_type);
        ArrayList<Type> mTypeList = new ArrayList<>();
        mTypeList.add(new Type(0, "Данные"));
        mTypeList.add(new Type(1, "Заметка"));
        TypeAdapter mAdapter = new TypeAdapter(mTypeList, new TypeAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(Type item) {
                switch ((int) item.getId()) {
                    case 0:
                        startActivity(new Intent(getApplicationContext(), AddNewDataActivity.class));
                        break;

                    case 1:
                        startActivity(new Intent(getApplicationContext(), AddNewNoteActivity.class));
                        break;

                    default:
                        break;
                }
            }
        });

        new ListConfig.Builder(mAdapter)
                .setHasFixedSize(true)
                .setHasNestedScroll(true)
                .setDefaultDividerEnabled(false)
                .build(getApplicationContext())
                .applyConfig(getApplicationContext(), mRvType);
    }
}

