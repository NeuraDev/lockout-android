package com.lockout.keys.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.lockout.keys.R;
import com.lockout.keys.helpers.AppData;
import com.lockout.keys.models.HeadersListModel;
import com.lockout.keys.models.Record;
import com.lockout.keys.models.TypeConnection;

import java.lang.reflect.Type;
import java.util.ArrayList;

/**
 * Created by agladkov on 31.03.17.
 */

public class HeaderAdapter extends RecyclerView.Adapter<HeaderAdapter.HeaderViewHolder> {
    private final String TAG = "RECORDADAPTER";
    private final OnItemClickListener listener;
    private ArrayList<TypeConnection> typeConnections;

    public interface OnItemClickListener {
        void onItemClick(TypeConnection item);
    }

    public HeaderAdapter(ArrayList<TypeConnection> typeConnections, OnItemClickListener listener) {
        this.typeConnections = typeConnections;
        this.listener = listener;
    }

    static class HeaderViewHolder extends RecyclerView.ViewHolder {
        private final TextView mTxtTitle;
        private final TextView mTxtType;
        private final ImageView mImgType;

        HeaderViewHolder(View itemView) {
            super(itemView);
            mTxtTitle = (TextView) itemView.findViewById(R.id.header_txt_title);
            mTxtType = (TextView) itemView.findViewById(R.id.header_txt_type);
            mImgType = (ImageView) itemView.findViewById(R.id.header_img_security);
        }

        void bind(final TypeConnection typeConnection, final OnItemClickListener listener) {
            mTxtTitle.setText(typeConnection.getNameinfo());
            mTxtType.setText(typeConnection.getTypeinfo());

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    listener.onItemClick(typeConnection);
                }
            });

            switch (Integer.valueOf(typeConnection.getSecurelvl())) {
                case AppData.SECURITY_DEFAULT:
                    mImgType.setImageResource(R.drawable.simple);
                    break;

                case AppData.SECURITY_STRONG:
                    mImgType.setImageResource(R.drawable.strong);
                    break;

                case AppData.SECURITY_MAX:
                    mImgType.setImageResource(R.drawable.strongest);
                    break;

                default:
                    mImgType.setVisibility(View.GONE);
            }
        }
    }

    @Override public HeaderViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.cell_header, viewGroup, false);
        return new HeaderViewHolder(v);
    }
    @Override public void onBindViewHolder(HeaderViewHolder holder, int position) {
        holder.bind(typeConnections.get(position), listener);
    }
    @Override public int getItemCount() {
        return typeConnections.size();
    }
}
