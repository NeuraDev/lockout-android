package com.lockout.keys.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.lockout.keys.R;
import com.lockout.keys.models.Restore;
import com.lockout.keys.models.Type;

import java.util.ArrayList;

/**
 * Created by agladkov on 31.03.17.
 */

public class RestoreAdapter extends RecyclerView.Adapter<RestoreAdapter.RestoreViewHolder> {
    private final String TAG = "RECORDADAPTER";
    private final OnItemClickListener listener;
    private ArrayList<Restore> restores;

    public interface OnItemClickListener {
        void onItemClick(Restore item);
    }

    public RestoreAdapter(ArrayList<Restore> restores, OnItemClickListener listener) {
        this.restores = restores;
        this.listener = listener;
    }

    static class RestoreViewHolder extends RecyclerView.ViewHolder {
        private final TextView mTxtTitle;
        private final TextView mTxtSubtitle;

        RestoreViewHolder(View itemView) {
            super(itemView);
            mTxtTitle = (TextView) itemView.findViewById(R.id.restore_txt_title);
            mTxtSubtitle = (TextView) itemView.findViewById(R.id.restore_txt_subtitle);
        }

        void bind(final Restore restore, final OnItemClickListener listener) {
            mTxtTitle.setText(restore.getTitle());
            mTxtSubtitle.setText(restore.getSubtitle());
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    listener.onItemClick(restore);
                }
            });
        }
    }

    @Override public RestoreViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.cell_restore, viewGroup, false);
        return new RestoreViewHolder(v);
    }
    @Override public void onBindViewHolder(RestoreViewHolder holder, int position) {
        holder.bind(restores.get(position), listener);
    }
    @Override public int getItemCount() {
        return restores.size();
    }
}
