package com.lockout.keys.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.lockout.keys.R;
import com.lockout.keys.helpers.AppData;
import com.lockout.keys.models.Type;
import com.lockout.keys.models.TypeConnection;

import java.util.ArrayList;
import java.util.StringTokenizer;

/**
 * Created by agladkov on 31.03.17.
 */

public class TypeAdapter extends RecyclerView.Adapter<TypeAdapter.TypeViewHolder> {
    private final String TAG = "RECORDADAPTER";
    private final OnItemClickListener listener;
    private ArrayList<Type> types;

    public interface OnItemClickListener {
        void onItemClick(Type item);
    }

    public TypeAdapter(ArrayList<Type> types, OnItemClickListener listener) {
        this.types = types;
        this.listener = listener;
    }

    static class TypeViewHolder extends RecyclerView.ViewHolder {
        private final TextView mTxtTitle;

        TypeViewHolder(View itemView) {
            super(itemView);
            mTxtTitle = (TextView) itemView.findViewById(R.id.type_txt_title);
        }

        void bind(final Type type, final OnItemClickListener listener) {
            mTxtTitle.setText(type.getTitle());
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    listener.onItemClick(type);
                }
            });
        }
    }

    @Override public TypeViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.cell_type, viewGroup, false);
        return new TypeViewHolder(v);
    }
    @Override public void onBindViewHolder(TypeViewHolder holder, int position) {
        holder.bind(types.get(position), listener);
    }
    @Override public int getItemCount() {
        return types.size();
    }
}
