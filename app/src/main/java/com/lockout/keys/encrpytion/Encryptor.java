package com.lockout.keys.encrpytion;

import android.util.Base64;
import android.util.Log;

import com.lockout.keys.Authentication;

import org.spongycastle.crypto.PBEParametersGenerator;
import org.spongycastle.crypto.digests.SHA256Digest;
import org.spongycastle.crypto.generators.PKCS5S2ParametersGenerator;
import org.spongycastle.crypto.params.KeyParameter;

import java.io.UnsupportedEncodingException;
import java.math.BigInteger;
import java.security.InvalidKeyException;
import java.security.KeyFactory;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.interfaces.RSAPublicKey;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.RSAPublicKeySpec;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

/**
 * Created by sunwi on 11.12.2015.
 */
public class Encryptor {

    public static String getPBKDF2 (String password, String salt) throws Exception {
        //Log.e("data", "password - " + password + " salt - " + salt);
        MessageDigest md = MessageDigest.getInstance("SHA-256");

        md.update(password.getBytes("UTF-8")); // Change this to "UTF-16" if needed
        byte[] digest = md.digest();

        //Log.e("salt", Authentication.bytesToHex(digest));

        PKCS5S2ParametersGenerator generator = new PKCS5S2ParametersGenerator(new SHA256Digest());
        generator.init(PBEParametersGenerator.PKCS5PasswordToUTF8Bytes(salt.toCharArray()), digest, 1000);
        KeyParameter key = (KeyParameter)generator.generateDerivedMacParameters(256);
        //Log.e("key", Base64.encodeToString(key.getKey(), Base64.DEFAULT));
        return Base64.encodeToString(key.getKey(), Base64.DEFAULT);
    }

    public static String MD5(String input) throws NoSuchAlgorithmException {
        return Base64.encodeToString(MessageDigest.getInstance("MD5").digest(input.getBytes()), Base64.DEFAULT);
    }

    public static String generateIv(String idDevice) {
        try {
            return MD5(idDevice);
        } catch (NoSuchAlgorithmException e) {
            return "";
        }
    }

    public static String encrypt(String key, String initVector, String value) {
        try {
            //key = key.replace("\n", "").replace("\r", "");
            initVector = initVector.replace("\n", "").replace("\r", "");
            Authentication a = new Authentication();
            IvParameterSpec iv = new IvParameterSpec(Base64.decode(initVector, Base64.DEFAULT));
            SecretKeySpec skeySpec = new SecretKeySpec(Base64.decode(key, Base64.DEFAULT), "AES");
            Cipher cipher = Cipher.getInstance("AES/CBC/PKCS7PADDING");
            cipher.init(Cipher.ENCRYPT_MODE, skeySpec, iv);
            byte[] encrypted = cipher.doFinal(value.getBytes());
            return Base64.encodeToString(encrypted, Base64.DEFAULT);
        } catch (Exception ex) {
            Log.e("Error", ex.getLocalizedMessage());
            return "Error";
        }
    }

    public static String decrypt(String key, String initVector, String encrypted) {
        try {

            IvParameterSpec iv = new IvParameterSpec(Base64.decode(initVector, Base64.DEFAULT));
            SecretKeySpec skeySpec = new SecretKeySpec(Base64.decode(key, Base64.DEFAULT), "AES");

            Cipher cipher = Cipher.getInstance("AES/CBC/PKCS7PADDING");
            cipher.init(Cipher.DECRYPT_MODE, skeySpec, iv);

            byte[] original = cipher.doFinal(Base64.decode(encrypted, Base64.DEFAULT));

            return new String(original);
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        return null;
    }

    public static String RSAEncrypt(String plain, String mod, String exponent) throws NoSuchAlgorithmException,
            NoSuchPaddingException, InvalidKeyException,
            IllegalBlockSizeException, BadPaddingException, InvalidKeySpecException, UnsupportedEncodingException {

        String modulusString = mod;
        String publicExponentString = exponent;

        byte[] modulusBytes = Base64.decode(modulusString, Base64.DEFAULT);
        byte[] exponentBytes = Base64.decode(publicExponentString, Base64.DEFAULT);
        BigInteger modulus = new BigInteger(1, modulusBytes);
        BigInteger publicExponent = new BigInteger(1, exponentBytes);

        RSAPublicKeySpec rsaPubKey = new RSAPublicKeySpec(modulus, publicExponent);
        KeyFactory fact = KeyFactory.getInstance("RSA");
        PublicKey pubKey = fact.generatePublic(rsaPubKey);
        Cipher cipher = Cipher.getInstance("RSA/ECB/OAEPWITHSHA-512ANDMGF1PADDING");
        cipher.init(Cipher.ENCRYPT_MODE, pubKey);

        byte[] plainBytes = Base64.decode(plain, Base64.DEFAULT);
        byte[] cipherData = cipher.doFinal(plainBytes);
        String encryptedStringBase64 = Base64.encodeToString(cipherData, Base64.DEFAULT);
        return encryptedStringBase64;
    }

    public static String RSADecrypt(String RSAPrivateKey, String encrypted)
            throws NoSuchAlgorithmException, InvalidKeySpecException, NoSuchPaddingException,
            InvalidKeyException, BadPaddingException, IllegalBlockSizeException {
        String result = "";
        byte[] pKeyByte = Base64.decode(RSAPrivateKey, Base64.DEFAULT);

        PrivateKey privateKey = KeyFactory.getInstance("RSA").generatePrivate(new PKCS8EncodedKeySpec(pKeyByte));
        Cipher cipher = Cipher.getInstance("RSA/ECB/OAEPWITHSHA-512ANDMGF1PADDING");
        cipher.init(Cipher.DECRYPT_MODE, privateKey);

        byte[] cipherData = cipher.doFinal(Base64.decode(encrypted, Base64.DEFAULT));
        result = Base64.encodeToString(cipherData, Base64.DEFAULT);
        return result;
    }

    public static String[] genKeyPair() throws NoSuchAlgorithmException, UnsupportedEncodingException {
        String[] S = new String[2];
        KeyPairGenerator keyPairGen = KeyPairGenerator.getInstance("RSA");
        KeyPair keyPair = keyPairGen.genKeyPair();
        byte[] privKey = keyPair.getPrivate().getEncoded();
        RSAPublicKey publicKey = (RSAPublicKey) keyPair.getPublic();

        BigInteger pM = publicKey.getModulus();
        BigInteger pE = publicKey.getPublicExponent();

        StringBuilder builder = new StringBuilder();
        StringBuilder pubbuilder = new StringBuilder();

        pubbuilder.append("<RSAKeyValue>\n");
        write(pubbuilder, "Modulus", pM);
        write(pubbuilder, "Exponent", pE);
        pubbuilder.append("</RSAKeyValue>");

        S[0] = Base64.encodeToString(privKey, Base64.DEFAULT);
        S[1] = Base64.encodeToString(pubbuilder.toString().getBytes("UTF-8"), Base64.DEFAULT);

        return S;
    }

    private static void write(StringBuilder builder, String tag, BigInteger bigInt) {
        builder.append("\t<");
        builder.append(tag);
        builder.append(">");
        builder.append(encode(bigInt));
        builder.append("</");
        builder.append(tag);
        builder.append(">\n");
    }

    private static String encode(BigInteger bigInt) {
        return new String(Base64.encodeToString(bigInt.toByteArray(), Base64.DEFAULT));
    }
}
