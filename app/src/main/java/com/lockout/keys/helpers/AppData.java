package com.lockout.keys.helpers;

import com.google.common.base.Strings;
import com.google.gson.internal.Streams;
import com.lockout.keys.models.TypeConnection;

/**
 * Created by agladkov on 31.03.17.
 */

public class AppData {
    public static final int SECURITY_DEFAULT = 0;
    public static final int SECURITY_STRONG = 1;
    public static final int SECURITY_MAX = 2;

    public static final int UI_LOADING = 0; // for loading state
    public static final int UI_VISIBLE = 1; // for visible state
    public static final int UI_NO_ITEMS = 2; // for no items state

    public static TypeConnection CURRENT_HEADER; // selected header

    public static String EMAIL; // current email
    public static String CODEWORD; // current codeword
    public static String DEVICE_ID; // current device id

    public static String NEW_EMAIL; // new email
    public static String NEW_PASSWORD; // new password

    public static String USER_ID; // server user id
    public static String SALT; // client salt PBKDF2
    public static String LOG_SALT; // client log salt PBKDF2

    public static int SOURCE_TYPE = 0;
    public static final int SOURCE_DELETE = 0;
    public static final int SOURCE_UPDATE_EMAIL = 1;
    public static final int SOURCe_UPDATE_PASSWORD = 2;
}
