package com.lockout.keys.helpers;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.util.Log;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.internal.Streams;
import com.lockout.keys.AskActivity;
import com.lockout.keys.ErrorDescription;
import com.lockout.keys.Pack;
import com.lockout.keys.R;
import com.lockout.keys.activities.LoggedInActivity;
import com.lockout.keys.activities.LoginActivity;
import com.lockout.keys.encrpytion.Encryptor;
import com.lockout.keys.http.RetrofitApi;
import com.lockout.keys.models.DataObject;
import com.lockout.keys.models.KeyWord;
import com.lockout.keys.models.SendObject;

import java.io.UnsupportedEncodingException;
import java.security.NoSuchAlgorithmException;
import java.text.SimpleDateFormat;
import java.util.Date;

import es.dmoral.toasty.Toasty;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by agladkov on 03.04.17.
 */

public class Keys {
    private static final String TAG = "KEYS";

    public static void startExchange(final LoginActivity loginActivity, final KeyWord keyWord) {
        DataObject sendData = new DataObject();
        SimpleDateFormat dateFormatUTC = new SimpleDateFormat("dd.MM.yyyy HH:mm:ss");
        sendData.dateSend = dateFormatUTC.format(new Date());
        final FileWork file = new FileWork(loginActivity.getApplicationContext());
        final String S[];
        try {
            S = Encryptor.genKeyPair();
        } catch (NoSuchAlgorithmException | UnsupportedEncodingException e) {
            Toasty.error(loginActivity.getApplicationContext(), e.getLocalizedMessage(), Toast.LENGTH_SHORT).show();
            loginActivity.setAvailableUI(true);
            return;
        }

        final Pack pack = new Pack(loginActivity.getApplicationContext());
        final Gson gson = new Gson();
        sendData.rsa_cokxml = S[1];
        RetrofitApi client = new RetrofitApi(loginActivity.getApplicationContext());
        final RetrofitApi.PostJson apiCall = client.getRetrofitApi().create(RetrofitApi.PostJson.class);

        final SharedPreferences settings = loginActivity.getSharedPreferences("UserData", 0);
        settings.edit()
                .putString("clientOpenKey", S[1])
                .putString("clientKeyPair", S[0])
                .apply();

        apiCall.post("tables/Key2Exchange", pack.packing(sendData))
                .enqueue(new Callback<JsonObject>() {
                    @Override
                    public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                        DataObject sendData = new DataObject();
                        SimpleDateFormat dateFormatUTC = new SimpleDateFormat("dd.MM.yyyy HH:mm:ss");
                        sendData.dateSend = dateFormatUTC.format(new Date());
                        file.writeToFile(settings.getString("clientKeyPair", null),
                                settings.getString("userid", null) + "ckp");

                        apiCall.post("tables/Key3Exchange", pack.packing(sendData))
                                .enqueue(new Callback<JsonObject>() {
                                    @Override
                                    public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                                        SendObject result = gson.fromJson(response.body(), SendObject.class);
                                        DataObject sendData = new DataObject();
                                        SimpleDateFormat dateFormatUTC = new SimpleDateFormat("dd.MM.yyyy HH:mm:ss");
                                        sendData.dateSend = dateFormatUTC.format(new Date());
                                        sendData.rsa_sokxml = settings.getString("serverOpenKey", "");
                                        sendData.rsa_cokxml = settings.getString("clientOpenKey", "");

                                        DataObject dataObject = pack.unpacking(response.body(), DataObject.class);
                                        settings.edit().putString("serverOpenKey", dataObject.rsa_sokxml).apply();
                                        file.writeToFile(dataObject.rsa_sokxml, settings.getString("userid", null) + "sok");

                                        apiCall.post("tables/Key4Exchange", pack.packing(sendData))
                                                .enqueue(new Callback<JsonObject>() {
                                                    @Override
                                                    public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                                                        apiCall.post("tables/Auth3", pack.packing(keyWord))
                                                                .enqueue(new Callback<JsonObject>() {
                                                                    @Override
                                                                    public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                                                                        ErrorDescription desc = pack.unpacking(response.body(), ErrorDescription.class);
                                                                        if (desc.Description.equals(loginActivity.getString(R.string.server_ask))) {
                                                                            loginActivity.setAvailableUI(true);
                                                                            loginActivity.startActivity(
                                                                                    new Intent(loginActivity.getApplicationContext(), AskActivity.class));
                                                                        } else {
                                                                            loginActivity.setAvailableUI(true);
                                                                            loginActivity.startActivity(
                                                                                    new Intent(loginActivity.getApplicationContext(),LoggedInActivity.class));
                                                                        }
                                                                    }

                                                                    @Override
                                                                    public void onFailure(Call<JsonObject> call, Throwable t) {
                                                                        Toasty.error(loginActivity.getApplicationContext(), t.getLocalizedMessage(), Toast.LENGTH_SHORT).show();
                                                                        loginActivity.setAvailableUI(true);
                                                                    }
                                                                });
                                                    }

                                                    @Override
                                                    public void onFailure(Call<JsonObject> call, Throwable t) {
                                                        Toasty.error(loginActivity.getApplicationContext(), t.getLocalizedMessage(), Toast.LENGTH_SHORT).show();
                                                        loginActivity.setAvailableUI(true);
                                                    }
                                                });
                                    }

                                    @Override
                                    public void onFailure(Call<JsonObject> call, Throwable t) {
                                        Toasty.error(loginActivity.getApplicationContext(), t.getLocalizedMessage(), Toast.LENGTH_SHORT).show();
                                        loginActivity.setAvailableUI(true);
                                    }
                                });
                    }

                    @Override
                    public void onFailure(Call<JsonObject> call, Throwable t) {
                        Toasty.error(loginActivity.getApplicationContext(), t.getLocalizedMessage(), Toast.LENGTH_SHORT).show();
                        loginActivity.setAvailableUI(true);
                    }
                });
    }
}
