package com.lockout.keys.helpers;

import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;

import com.lockout.keys.Authentication;
import com.lockout.keys.R;
import com.lockout.keys.models.Auth1Output;

import java.math.BigInteger;

import es.dmoral.toasty.Toasty;

import static android.content.Context.INPUT_METHOD_SERVICE;

/**
 * Created by agladkov on 04.04.17.
 */

public class Utilities {
    public static String convertTime(String value) {
        switch (value) {
            case "5 минут":
                return "5";

            case "30 минут":
                return "30";

            default:
                return "60";
        }
    }

    public static void SRV1Step(Auth1Output loginAuth, Auth1Output auth, Authentication authentication) {
        authentication.getN(AppData.EMAIL, auth.s);
        loginAuth.a = authentication.getA().toString();
    }

    public static void SRV2Step(Auth1Output loginAuth, Auth1Output auth, String email,
                           String password, Authentication authentication) {
        authentication.getU(auth.b);
        String x = authentication.getX(password, auth.s).toString();
        authentication.getk();

        authentication.getS(auth.b, authentication.g.toString(), x);
        authentication.getK();
        loginAuth.b = auth.b;
        authentication.getV(x);
        loginAuth.id_device = AppData.DEVICE_ID;
        loginAuth.m = authentication.getM(authentication.N, authentication.g,
                email, auth.s, authentication.A,
                auth.b, authentication.K).toString();
    }

    public static boolean SRVCheckR(Auth1Output loginAuth, Authentication authentication) {
        BigInteger myR = authentication.getR(authentication.A, authentication.M, authentication.K);
        BigInteger serverR = new BigInteger(loginAuth.r);
        return (myR.equals(serverR));
    }

    public static boolean validateCode(int statusCode) {
        return statusCode == 200 || statusCode == 201;
    }

    public static void setupHideKeyboard(final AppCompatActivity activity, View view) {
        view.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                try {
                    InputMethodManager imm = (InputMethodManager) activity.getSystemService(INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(activity.getCurrentFocus().getWindowToken(), 0);
                } catch (NullPointerException e) {
                    Log.e(activity.getLocalClassName(), "Bad window token - " + e.getLocalizedMessage());
                }
                return false;
            }
        });
    }
}
