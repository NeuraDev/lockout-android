package com.lockout.keys.http;

/**
 * Created by sunwi on 01.05.2017.
 */

public class ApiName {
    public final static String BUY = "tables/PayBitch";
    public final static String UPDATE_USERNAME_PHONE = "tables/UpdateUP";
    public final static String LOAD_DATA = "tables/ReadData";
    public final static String DELETE_DATA = "tables/Delete";
    public final static String UPDATE_DATA = "tables/UpdateData";
    public final static String LOAD_NOTE = "tables/ReadNote";
    public final static String DELETE_NOTE = "tables/Delete";
    public final static String UPDATE_NOTE = "tables/UpdateData";

    public final static String RESTORE_SMS = "tables/SMS";

    public final static String AUTH_START = "tables/Auth0";
    public final static String AUTH_SECOND = "tables/Auth1";
}
