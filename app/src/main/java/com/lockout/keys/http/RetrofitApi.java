package com.lockout.keys.http;

import android.content.Context;

import com.google.gson.JsonObject;
import com.squareup.okhttp.Interceptor;
import com.squareup.okhttp.RequestBody;
import com.squareup.okhttp.ResponseBody;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Path;

/**
 * Created by agladkov on 03.04.17.
 */

public class RetrofitApi {
    private Retrofit retrofitApi;
    private Context context;

    public RetrofitApi(Context context) {
        this.context = context;

        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);

        OkHttpClient okHttpClient = new OkHttpClient.Builder()
                .addInterceptor(interceptor)
                .build();

        this.retrofitApi = new Retrofit.Builder()
                .baseUrl("https://lockout.azure-mobile.net/")
                .addConverterFactory(GsonConverterFactory.create())
                .client(okHttpClient)
                .build();
    }

    public interface GetRequest {
        @GET("path")
        Call<ResponseBody> get(@Path("path") String path);
    }

    public interface PostRequest {
        @POST("{path}")
        Call<String> post(@Path("path") String path, @Body JsonObject body);
    }

    public interface PostJson {
        @POST("{path}")
        Call<JsonObject> post(@Path("path") String path, @Body JsonObject body);
    }

    public Retrofit getRetrofitApi() {
        return retrofitApi;
    }
    public void setRetrofitApi(Retrofit retrofitApi) {
        this.retrofitApi = retrofitApi;
    }
}
