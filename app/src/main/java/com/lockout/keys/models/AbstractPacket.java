package com.lockout.keys.models;

/**
 * Created by sunwi on 12.12.2015.
 * Fabric root class
 */
public class AbstractPacket {
    public String dateSend;
    public String __version;
    public String __createdAt;
    public String __updatedAt;
    public boolean __deleted;
}
