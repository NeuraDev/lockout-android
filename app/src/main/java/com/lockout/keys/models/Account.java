package com.lockout.keys.models;

import com.lockout.keys.models.AbstractPacket;

/**
 * Created by sunwi on 14.12.2015.
 */
public class Account extends AbstractPacket {
    public String username;
    public String password;
    public String email;
    public String salt;
    public String codeword;
    public String phone;
    public String id;
    public String SecretTOTP;
    public String GridLink;
}
