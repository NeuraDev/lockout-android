package com.lockout.keys.models;

import com.lockout.keys.models.AbstractPacket;

/**
 * Created by sunwi on 09.12.2015.
 */
public class Auth1Output extends AbstractPacket {
    public String i;
    public String a;
    public String s;
    public String b;
    public String m;
    public String r;
    public String id_device;
    public String id;
    public String username;
    public String dataid;
    public String date;

    public Auth1Output() {
    }

    public Auth1Output(String i, String a, String s, String b, String m, String r, String id_device,
                       String id, String username, String dataid, String date) {
        this.i = i;
        this.a = a;
        this.s = s;
        this.b = b;
        this.m = m;
        this.r = r;
        this.id_device = id_device;
        this.id = id;
        this.username = username;
        this.dataid = dataid;
        this.date = date;
    }
}
