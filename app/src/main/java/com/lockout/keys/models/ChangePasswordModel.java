package com.lockout.keys.models;

import java.util.ArrayList;

/**
 * Created by sunwi on 24.12.2015.
 */
public class ChangePasswordModel extends AbstractPacket {
    public String password;
    public String email;
    public String salt;
    public String id;
    public ArrayList<RelationDataModel> data;
}
