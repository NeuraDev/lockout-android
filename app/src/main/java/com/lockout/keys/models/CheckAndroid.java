package com.lockout.keys.models;

import com.lockout.keys.models.AbstractPacket;

/**
 * Created by sunwi on 24.04.2016.
 */
public class CheckAndroid extends AbstractPacket {
    public String purchaseInfo;
    public String signature;
    public String purchaseTime;
    public String orderId;
}
