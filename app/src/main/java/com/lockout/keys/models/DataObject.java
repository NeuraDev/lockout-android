package com.lockout.keys.models;

import com.google.gson.annotations.SerializedName;
import com.lockout.keys.models.AbstractPacket;

/**
 * Created by sunwi on 19.12.2015.
 */
public class DataObject extends AbstractPacket {
    @SerializedName("RSA_COKXML")
    public String rsa_cokxml;
    @SerializedName("RSA_SOKXML")
    public String rsa_sokxml;
}
