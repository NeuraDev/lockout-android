package com.lockout.keys.models;

import com.lockout.keys.models.AbstractPacket;

/**
 * Created by sunwi on 21.12.2015.
 */
public class DataTable extends AbstractPacket {
    public String loginfo;
    public String urlinfo;
    public String passinfo;
}
