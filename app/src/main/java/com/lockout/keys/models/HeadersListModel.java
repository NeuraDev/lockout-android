package com.lockout.keys.models;

import java.util.List;

/**
 * Created by sunwi on 14.12.2015.
 */
public class HeadersListModel extends AbstractPacket {
    public List<TypeConnection> headers;
    public List<RelationDataModel> data;
    public String deviceid;
    public String dataid;
}
