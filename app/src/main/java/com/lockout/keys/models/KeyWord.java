package com.lockout.keys.models;

import com.lockout.keys.encrpytion.Encryptor;
import com.lockout.keys.helpers.AppData;
import com.lockout.keys.helpers.Utilities;
import com.lockout.keys.models.AbstractPacket;

import java.util.ArrayList;

/**
 * Created by sunwi on 12.12.2015.
 */
public class KeyWord extends AbstractPacket {
    private String dataid;
    private String userid;
    private String deviceid;
    private String data;
    private String date;
    private String keyword;

    public KeyWord(String dataid, String userid, String deviceid, String data, String date, String keyword) {
        this.dataid = dataid;
        this.userid = userid;
        this.deviceid = deviceid;
        this.data = data;
        this.date = date;
        this.keyword = keyword;
    }

    public KeyWord() {
    }

    public static KeyWord createInstance(String logsalt, ArrayList<String> timeArray,
                                         String date, String userid, String keyValue) {
        KeyWord keyWord = new KeyWord();
        keyWord.data = Encryptor.encrypt(
                logsalt,
                Encryptor.generateIv(AppData.EMAIL),
                timeArray.toString());
        keyWord.deviceid = AppData.DEVICE_ID;
        keyWord.userid = userid;
        keyWord.dataid = AppData.CURRENT_HEADER.getDataid();

        keyWord.date = date;
        keyWord.keyword = keyValue;

        return keyWord;
    }

    public String getDataid() {
        return dataid;
    }

    public void setDataid(String dataid) {
        this.dataid = dataid;
    }

    public String getUserid() {
        return userid;
    }

    public void setUserid(String userid) {
        this.userid = userid;
    }

    public String getDeviceid() {
        return deviceid;
    }

    public void setDeviceid(String deviceid) {
        this.deviceid = deviceid;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getKeyword() {
        return keyword;
    }

    public void setKeyword(String keyword) {
        this.keyword = keyword;
    }
}
