package com.lockout.keys.models;

/**
 * Created by sunwi on 25.12.2015.
 */
public class Model {

    private String name;
    private String subname;
    private String type;

    public Model(String name, String subname, String type) {
        this.name = name;
        this.subname = subname;
        this.type = type;
    }

    public String getName() {
        return name;
    }
    public String getSubname() {
        return subname;
    }
    public String getType() {
        return type;
    }
    public void setName(String name) {
        this.name = name;
    }
    public void setSubname(String subname) {
        this.subname = subname;
    }
    public void setType(String type) {
        this.type = type;
    }
}
