package com.lockout.keys.models;

/**
 * Created by agladkov on 31.03.17.
 */

public class Record {
    private long id;
    private long security;
    private String type;
    private String title;

    public Record(long id, long security, String type, String title) {
        this.id = id;
        this.security = security;
        this.type = type;
        this.title = title;
    }

    public long getId() {
        return id;
    }
    public void setId(long id) {
        this.id = id;
    }
    public long getSecurity() {
        return security;
    }
    public void setSecurity(long security) {
        this.security = security;
    }
    public String getType() {
        return type;
    }
    public void setType(String type) {
        this.type = type;
    }
    public String getTitle() {
        return title;
    }
    public void setTitle(String title) {
        this.title = title;
    }
}
