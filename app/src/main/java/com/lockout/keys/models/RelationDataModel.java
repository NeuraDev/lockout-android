package com.lockout.keys.models;

import com.google.gson.annotations.SerializedName;
import com.lockout.keys.models.AbstractPacket;

/**
 * Created by sunwi on 14.12.2015.
 */
public class RelationDataModel extends AbstractPacket {
    //Type Connection
    @SerializedName("nameinfo")
    public String nameinfo;
    public String securelvl;
    public String checkinfo;
    public String typeinfo;
    public String dataid;
    public String userid;

    //Data Table
    public String loginfo;
    public String urlinfo;
    public String passinfo;

    //Note Table
    public String noteinfo;
}