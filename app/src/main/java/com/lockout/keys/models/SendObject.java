package com.lockout.keys.models;

/**
 * Created by sunwi on 12.12.2015.
 */
public class SendObject {
    public String token;
    public String aesenc;
    public String aeskey;
    public String iddevice;
}
