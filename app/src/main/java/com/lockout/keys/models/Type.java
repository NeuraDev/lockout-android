package com.lockout.keys.models;

/**
 * Created by agladkov on 04.04.17.
 */

public class Type {
    private long id;
    private String title;

    public Type(long id, String title) {
        this.id = id;
        this.title = title;
    }

    public long getId() {
        return id;
    }
    public void setId(long id) {
        this.id = id;
    }
    public String getTitle() {
        return title;
    }
    public void setTitle(String title) {
        this.title = title;
    }
}
