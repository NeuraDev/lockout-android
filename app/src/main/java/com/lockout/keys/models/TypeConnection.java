package com.lockout.keys.models;


/**
 * Created by sunwi on 14.12.2015.
 */
public class TypeConnection {
    private String nameinfo;
    private String securelvl;
    private String checkinfo;
    private String typeinfo;
    private String dataid;
    public String userid;

    public static TypeConnection Create(RelationDataModel relationModel) {
        TypeConnection tConnection = new TypeConnection();
        tConnection.checkinfo = relationModel.checkinfo;
        tConnection.nameinfo = relationModel.nameinfo;
        tConnection.typeinfo = relationModel.typeinfo;
        tConnection.securelvl = relationModel.securelvl;
        tConnection.userid = relationModel.userid;
        return tConnection;
    }

    public String getNameinfo() {
        return nameinfo;
    }
    public void setNameinfo(String nameinfo) {
        this.nameinfo = nameinfo;
    }
    public String getSecurelvl() {
        return securelvl;
    }
    public void setSecurelvl(String securelvl) {
        this.securelvl = securelvl;
    }
    public String getCheckinfo() {
        return checkinfo;
    }
    public void setCheckinfo(String checkinfo) {
        this.checkinfo = checkinfo;
    }
    public String getTypeinfo() {
        return typeinfo;
    }
    public void setTypeinfo(String typeinfo) {
        this.typeinfo = typeinfo;
    }
    public String getDataid() {
        return dataid;
    }
    public void setDataid(String dataid) {
        this.dataid = dataid;
    }
    public String getUserid() {
        return userid;
    }
    public void setUserid(String userid) {
        this.userid = userid;
    }
}
